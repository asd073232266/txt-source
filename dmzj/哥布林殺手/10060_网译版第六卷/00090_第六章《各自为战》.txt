“哎呀哎呀，终於结束了啊……”

最初注意到“那个”的是，一个工人。

他看着远远透射过来缓缓沉下的夕阳，将铲子（Schop）扛在肩膀上，然後大大地叹了一口气。他是个微不足道的男人，也不想成为商家的佣人，但也没有可以游手好闲惶惶终日的金钱。结果就变成这样手裡拿着铲子，汗流浃背地工作，但还是不断抱怨不满的男人。

──可恶、真好啊、女冒险者。

决不是那种裹得严严实实的无趣样子，而是穿着容易活动的轻薄衣物来回走动。不然的话就是套着像是魔法使或者圣职者等人穿的宽松长袍的女人们。

和那些浓妆艳抹的卖身女都不一样。当然，如果真的是高级娼妓，又会是另一个层次了，但男人自然与这类人无缘。

然後和这样的女人寝食与共的冒险者们。肯定是很快活吧。像这样随性而活，还真是羡慕啊。

“还真是个舒服的活啊。只要杀掉怪物夺取宝物（Hack and Slash）就可以这麼有钱吗。”

这当然不是那麼简单的东西，就算是这个男人也清楚。但，无论是谁，谁都会觉得“只有自己不一样” “只有自己会成功”。然後想当然的，就只会看到事物的“便利的一面”。

妄想着成为一个冒险者的男人，就正是如此。不能非常成功也没关係，不能成为勇者也无所谓。只要穿上颇为優质的装备，救助一两个村子，被村姑感谢什麼的也不错……。

还有，也可以用钱买来落为奴隷的贵族小姐，然後照顾她什麼的。也带着漂亮的美人魔法师，再逐渐增加同伴。当然也要是美丽的女人们。

像是想不到其他的什麼了的──因为男人也不太清楚──漏洞百出的想法。最後，和喜欢的女人成家脱身引退，“这就是冒险啊！”什麼的。

“……嘿嘿。”

他想着的“随随便便的成功”放到现实裡来说是绝对不可能是“随随便便”的，倒不如说根本闻所未闻。只是用来取悦自己的无端妄想罢了。

但这也不是什麼会被谁斥责的事情。也没有损伤什麼他人的利益。只是工作，喝酒，吃饭，抱女人，和朋友一起玩乐，吐着牢骚，时常幻想，并就这样活下去。

这就行了。

“……嗯?”

然後，最初注意到“那个”的，是他。

在训练场──已经把栅栏也围好，差不多要建完的一角。有一堆没有印象的沙土。因为即使是沙土也是有用的建筑材料，所以挖出来的沙土必须要堆在指定的位置，这是规矩。

“真是，是有人偷懒了吗?”

也不是不知道有人会觉得麻烦的心情，他有时也会偷偷地把一些沙土堆在一旁。可是一旦发现了这种情况，就必须要自己来处理，真是有点不爽。

想着要不要乾脆当做没有看见，但不凑巧的是他手上正好拿着一把铲子。

“……真没辙。”

唉，就动一点吧。与其明天带着烦躁的心情去做，还不如现在就做掉，也能睡个好觉吧。

这麼想着的他走近那堆土山，突然看到那边有一个微微动着的人影。夕阳的餘晖，映照出那个小孩子身形──但是容貌醜陋的生物，正在像是匍匐着一样蠢动。

──哥布林!?

在那时，他没有漏出声音来理应被称讚。而他接下来採取的应对方式，也绝不应该被人多加口舌。

他用双手紧握着铲子，然後小心翼翼地轻轻踱步慢慢接近，然後高举起铲子。

“G R O B !?”

被沙土磨得光亮的铲子尖端，以甚至可以与战斧媲美的威力，轻易地击碎了小鬼的头颅。男人将喷洒着黑乎乎的血液和脑浆无力倒地的哥布林的尸体，狠狠地踩了一脚。

“哈哈！怎麼样，活该……”

他这样说着，把铲子给提了起来，带起幾条黏糊糊的血丝，男人皱起了眉头。

冷静地想一想，这是明天还得用的工具。必须把血糊给洗掉。但是，他胸中又有一股与嫌恶感一併而来的，像是回应刚才期待似的漂亮地把哥布林的脑袋给割下来的成就感。

“话说回来，这傢伙是从哪裡进……是挖洞进来的?”

一边挥着铲子上的血，男人一边偷偷地往洞穴裡面窥视着。

那是个虽然看上去粗糙，实际上却颇为牢固的竖坑。是哥布林挖的吧。洞穴深不见底──不，不只是因为洞裡黑黝黝的。太阳也不知什麼时候沉了下去。

“……”

男人突然颤了一下，一股不详的恐惧感掠过後背。

“不，不不，我不用去调查。这裡有冒险者在。”

交给他们吧。这不是自己的分内之事。话虽如此，但必须得去报告情况。

就在这时候。

“啊……啊!?”

刚想着要走，右足突然一阵钝痛，视野一倾跌倒在地上。什……？他这样想着，强行扭过身体来看，从脚踝那裡汨汨地渗出鲜血来。

“G R O B ! G R O O R B !!”

然後再是，一只手上沾着正体不明的黏液的短剑的──不，小声發出嘲笑声的小鬼，十只、二十只、接连从夜影中现身。

“…… ── ──── ……”

男人像是要求助般的张开口，但舌头好像打结了一样颤抖着，说不出一个词来。从被刺伤的脚踝传来仿佛是被叮咬了一样的麻痹感和疼痛。喉咙裡火辣辣的。嘴裡粘着黏糊糊的，铁锈的味道。无法呼吸。视野变得一片空白。

哥布林不只一只，他没有注意到。因此，他当然也没有意识到他是被哥布林用毒短剑给刺中了。

不一会功夫，他就死了。

但是理所当然的，今晚死的──他是最初的一个，但绝不是最後一个。

§

“不發出声响杀死哥布林的方法有八种──……”

就在哥布林杀手向新人们这样说着的时候，响起了悲鸣。

已是黄昏时分，没有全部回街道的也只有冒险者了。虽说在晚上进行冒险非常危险，但即使不这样，在像是遗迹啊迷宫啊洞穴啊这种地方，也十分昏暗。

所以像这样依靠双月和星星的光亮，进行夜间训练绝对不是白费。至少有着这样想法的冒险者──红髮少年和圃人少女，还有新手战士和见习圣女。还有其他的十餘名，在其它冒险者们已经返回之後，聚集到了训练场的广场。

“怎，怎麼了……?”

“悲鸣声……吧。”

年少的冒险者们纷纷面面相觑，开始嘀咕了起来。

“……”

只有哥布林杀手把手放到佩在腰间的剑上，把剑给拔了出来。

他的行动非常快。同时惊慌失措的少年少女们向周围看去，并寻找着悲鸣的来源。悲鸣不止一次。就在这当口，两次，三次。

“喂，喂，不是發生了什麼事吗……!?”

“别吵!”

红髮的少年不解地开口，哥布林杀手止住了他。

“架起防御，围着咒术士组成半圆阵，前卫、拿起武器。”

“了解”，新手战士脸上带着紧张的表情把见习圣女护在背後。”

“……呐，不是训练吧，这个。”

“就算是。”哥布林杀手短短地说道。“也不要松懈。”

“嗯……啊……不要这样说啦！我都不知道是要害怕，还是不害怕了啦!”

發出啊哈哈的乾笑声，圃人少女也拿起小小的剑和盾牌，摆好架势。但表情还是十分僵硬，即使在夜色中，发青的脸色仍清楚可见。

恐怖，紧张，无外乎就是这两者。她那不长但尖尖的耳朵，微微颤抖着。

“嘁……！”

咂舌的是红髮的少年。

他把拐杖一挥，指向还没有完全弄清情况的其他新人们。

“喂，没听见吗！不要呆着不动！组成阵形！”

“哦，哦哦……”

“我知道了，知道了……!”

是因为这不是前辈，而是同辈的话语吗。思考没有跟上，没能弄明事态的新人们，终於开始行动了。他们各自持着自己的武器，一边笨拙地行动着，开始靠墙架起半圆形的阵型。

“喂，那边的，把盾牌架好！要保护旁边的人和身後的人!”

见习圣女呼喊着，为不熟悉的成员鼓气。

这麼说来，新手战士们也好见习圣女也好，就算只治退过巨鼠，但也算是有实战经验。圃人少女还有红髮少年也一样。这是从初学者开始踏踏实实地走出一步的证据吧。

这样一步、两步、三步的话──……。

“……”

看到那个样子，哥布林杀手像是突然悟到了什麼，低低地念了一声。

应该是要把新人们放着去确认事态呢，还是应该继续保护他们呢。在这迷惑之上……又觉得不能把他们放着不管。

──愚蠢的想法。

自己也觉得很不可思议。在这种情况下，要是不快点收集情报，就与坐等全灭无异。说起来自己考虑这件事本身就是在浪费时间。也没必要再多想了。

“在这裡待机。”

下定结论的哥布林杀手，环顾着他们，毫不犹豫地说道。

“如果我过了十五分钟没有回来，就独自行动。”

“独自……”

“我应该是死了，即使不是这样也是受了无法行动的伤了吧。”哥布林杀手淡淡地说道。

努力地无视着新人们的喧闹。

“回到街道为最優先，但如果困难的话，就坚持到早上。”

然後他跑了出去。没有回头直直地跑了出去。

悲鸣声还在不断增加。呐喊，怒吼。武器碰撞声。剑戟交错声。不知不觉间，各种声音向四面八方扩散开来，仿佛要压过来一般将他包围起来。

在春天的夜晚，还留着些许冰精灵气息的刺骨黑暗中，无法準确地判别事态。还在建的房屋的影子大的有点令人毛骨悚然，而哥布林杀手则是吐了一口气。

──不。

“……一。”

哥布林杀手飞奔着的同时，随手将右手上的剑投了出去。建设中的设施旁边的建材堆裡的影子。飞出的剑刃毫无偏误地命中其要害。

跑进阴暗处的哥布林杀手，踩住喉咙被贯穿而气绝身亡的小鬼的腹部，拔出剑。被血染得黑红的铲子，从毙命的哥布林手上掉落下来，發出喀啦喀啦的声音。

“果然、哥布林吗。”

在这短短的话语中到底包含了多少含义呢。

潜伏在黑暗中的小鬼，还有两个。即使黑暗中看不到，他的眼眸也能看穿。脚底感觉到一股粘稠的触感──同时还有扑鼻而来的铁锈气味。

那是一名像是伏在地上一样毙命的，年轻的冒险者。不知道职业。也不知道年龄和种族。

那个冒险者没有脸。从头顶到脸部，都被锐物毫不留情地割了下来。但从那微微膨胀的胸口，和不断痉挛着的手足的线条来看，是女性。

“G O R O R O B !!”

“G R O O O  O R O R B !”

哥布林發出尖叫声跳了过来，哥布林杀手沉默着把剑挥了过去。响起金属和金属碰撞的清澈声音。哥布林的手上拿着鹤嘴锄。是抢过来的吧。

哥布林杀手毫不犹豫地踏出一步，单手用剑压住鹤嘴锄。

“G R O B !!”

还有一只，也拿着鹤嘴锄的哥布林，毫不留情地把锋利的尖端朝哥布林杀手挥来。

“……哼。”

钢铁制的鹤嘴，贯穿了哥布林杀手举起的盾牌。十字镐原本就是贯穿力极强的武器。

但是，这样也好。

哥布林杀手把左手硬是一扭，把穿在盾牌上的鹤嘴锄从小鬼手裡夺下来。同时，他不假思索地向右边的哥布林抬起腿，一脚踹中它的股间。

“G R O O O R O R O R O R B ! ? !”

“二。”

脚尖感觉到一股什麼东西被踹烂了的令人嫌恶的感触，同时传来一阵混浊的哀嚎。一边踩着痛得几近昏厥的哥布林的头颅，哥布林杀手一边抬起剑来。

向着左方，刚刚武器被夺走仓皇逃跑的哥布林的後背，毫不犹豫地投了出去。

“G R O O R B !?”

“那麼……”

即使没有当场死亡，只要那沉重的铁块砸碎脊髓，身体也就无法动弹了。

哥布林杀手把在脚底下不停挣扎着的哥布林的後脑勺，狠狠地踩碎。就像踩碎腐烂的果实一样的讨厌感觉。

把血液和脑浆甩掉，向前走。

他用剑刺进了抖动痉挛着的哥布林的延髓，让它的呼吸给彻底停了下来。

“三。”

然後哥布林杀手强行拔出穿在盾牌上的鹤嘴锄。鹤嘴上还沾着新鲜的泥土。是从哪裡挖洞过来，袭击了这个训练场的吧。

是计划要挖到这裡来袭击这个地方的吧。是想要杀光这个地方的人的吧。

哥布林。

哥布林。

哥布林。

厌恶。所有的一切都厌恶。

天翻地转。

尸体有四具。哥布林三。冒险者一。

像十年前的夜晚一样。

已经逃脱不了了。这不是已经再清楚不过了吗？

自己是哥布林杀手。

“……是谁，在那裡……！”

就在此时。

伴随着尖锐的诘问声，新的人影──冒险者向阴影出冲过来。

也难怪。在黑暗中，飘着血腥味，拿着武器的人站在那裡，谁都会如此吧。

但是，这个拿着锡杖的冒险者，一看出了对方的真实身份，就破颜而笑。

“哥布林杀手先生！”

“没事吗。”

“是的。”

她用双手紧紧握住锡杖，喜形于色地上下点头。

“今天也是治疗的工作。因为用完了奇迹，所以就在房间裡休息……”

她的视线扫过地上毙命的小鬼……然後落在冒险者的尸体上。形状姣好的眉头皱了起来。女神官也不在意白色法衣被血沾染，双膝跪下，握住反射性持续痉挛着的死者的手。

“是哥布林吗。”

“啊啊。”

哥布林杀手也不去看那边，只是把剑上的血挥去。

“还有奇迹吗。”

“……休息过了。还可以，祈求三次。”

“其他的”哥布林杀手微微顿了一下，“……同伴，有来吗?”

“大概……”

“好。”

哥布林杀手正眼看向女神官。

女神官也抬头望向哥布林杀手。模糊的月光射下来，照耀着她蓝色的眼瞳。

清澈透明，像玻璃球一样，哥布林杀手突然这麼想道。

“能走吗。”

“……走吧。”她咬着嘴唇，用颤抖的声音说道。

也不用擦拭眼角。因为她没有哭。

“走吧……!”

“啊啊。”

哥布林杀手点点头。

“哥布林，就要全部杀光。”

§

不久，两人到达了即将完成的，作为训练场的办事处的建筑物。

虽说是整个训练场的中枢，但现在还未完成，倒不如说更像是个聊胜于废墟的地方。屋顶和墙壁都有很多缺漏的地方，可以看到手裡拿着武具聚集起来的那些冒险者的身影。

摆脱困境而来到这裡的冒险者，幸运地还有挺多人。

“喂，喂，这不是是哥布林杀手吗！你那边没事吗！”

最先出声的是，在门口警戒周围的长枪手。一直是冲在前头的他居然留下来了，这让人着实感到有些意外。

“啊啊。”

哥布林杀手重重地点了点头。他自然不会误解长枪手发问的意思。

“我管的人没事。”

“是吗。其他大部分人都因为是傍晚了就回去了。”

“赶在……天暗……之前……对吧?”

然後，还有一人。

那是在长枪手旁边、如影子般贴着的性感美丽的魔女，周围漂浮着淡淡的光球。

《鬼火（Will O' Wisp）》……不，没有精灵。是《光明（Light）》咒术吗。

虽然那是法术生成的火焰，但在这种情况下，谁也不会去费尽心思的生火。春天的夜晚风很大。如果發生火灾，“这”就会是起因。

“两个人都平安无事吗……”

不知是否是因为和熟人相遇，女神官总算稍稍松了一口气。拼命地止住直到现在都在颤抖的膝盖，双手紧紧握着锡杖。

“我们也在哦！”

就像是要给她鼓劲一般，清脆爽朗的声音响起。女神官一听到那个声音，脸上的表情就一下子像花朵绽放一般变成了笑容。

“各位！”

“呀，来了呐。虽说是常在战场，但这种情况还真是想不到啊。”

“托你的福，晚饭也吃得很饱。”

一如既往的蜥蜴僧侣，和漫不经心地抚着肚子的矿人道士。妖精弓手则是把禁不住跑出来的女神官紧紧地一把抱住。

“你那边没事吗？没有受伤吧？没有被哥布林做什麼奇怪的事吧？”

“不要紧的、不要紧的，太好了，大家都没事……!”

────没有和那个时候一样，真是太好了。

被同伴们所包围着，女神官的眼角不由自主地渗出了眼泪。但又有谁会来责备她。两次、三次，大概也没有人能再度忍受失去同伴的痛苦了吧。

“……”

哥布林杀手盯着那光景看了数秒，慢慢地转过头思索起来。

总之就是一般要考虑的事情。应该做什麼。能做什麼。

这座建筑物还未竣工，很脆弱。不可能这样长时间的笼守的吧。那麼就需要战鬥力。那些在角落裡胆怯畏缩的新手可派不上用场。

然後──……。

“哦，这边也没事吗?哥布林杀手。”

视线转向那个轻轻抬起一只手的巨汉。

看来重战士已经是战鬥过了，身上微微地飘着一股血腥味。当然，被杀的是哥布林，自不必说。

哥布林杀手向四周张望，确认找不到其他的熟人。

“今天一个人吗。”

“那傢伙是个女人，总有不能动的日子。我们队伍裡的小鬼在旅馆裡照顾她。”

重战士，带着一副无法形容的深奥表情这样说着，耸了耸肩膀，身上的甲胄与武具都跟着摇晃。

“还真是头痛，同伴的身体管理也是工作的一部分呢。”

不过这也算是立功了──也可以这麼说吧。就结果来说，因为以身体状况不良为理由休假，所以他的团队（Party）也没有被卷入其中。

“可是啊……”

重战士的脸上浮现出了如鲨鱼般凶猛的笑容。

“边境第一的傢伙聚集了三个人，那岂不是就有趣起来了吗。”

当然──要说情况是否有所寰转，也仍然没有。

没有到达这裡的冒险者们的临终惨叫，伴随着哥布林们的叫声响起，到达这裡的新人冒险者纷纷露出了恐惧的表情。

冒险者，幾乎都是處於“袭击的一方”，而不是“被袭击的一方”。当然，有时也会有被袭击的情况，也会接下护卫商队之类的委托。但果然，在内心深处，认为自己不可能被袭击的这种意识依然根深蒂固。

想不到会成为自己被袭击的一方。从改变这一认识的角度来看，女神官虽然不幸，但也可说是幸运。

不管怎麼逃──与否，不打退哥布林，就撑不到明天。

众人想必也都是一样的认知吧，长枪手向外窥视着皱起了眉头。

“照这样下去的话就麻烦了。在这裡死守也不是办法。”（原文：城を枕に討ち死に，日本战国时期一位城主被岛津攻城，籠城死守不敌投降自裁的典故。）

“总、之……，先和、他们……汇合……比较、好吧。”

“啊啊。”哥布林杀手点了点头。“我负责的那些人，在广场上待机。”

“那就传令。”重战士很快地说道。“已经抓住情况了。是哥布林。过来这裡，也要让其它还活着的傢伙知道。”

“那我去吧！”妖精弓手马上举起手。“这裡面我跑得最快吧!”

“好，那就拜托了。”

“交给我吧!”

妖精弓手说完就如一阵疾风般的消失在夜幕中。目送着她背影的重战士，接着又环视了一下众人。

小鬼杀手与其团队（Party）里的五人。长枪手和魔女。还有自己。虽然在新手中多少会有排得上用场的……但能成为战鬥力大概也就十名左右。胆怯畏缩的人则不在考虑之内。重战士爽快地做出判断，然後话锋一转。

“那麼，哥布林杀手，对手是哥布林，你来指挥如何？”

“应该是，哥布林吧。”

哥布林杀手的回答没有一丝迷茫。

“是有上位种吧，应该不是王。从卖弄这种小聪明来看，是萨满吧。”

“有依据吗?”

“如果是哥布林以外的什麼来指挥的话，哥布林只会作为杂兵，不可能是主力。”

这是事实。

特意挖洞过来袭击训练场什麼的，除了小鬼之外，也无作他想。

重战士点了点头，得出结论，“杂鱼也要击溃，同时也要捣毁敌人的本阵。”

“敌人的本阵在哪裡?”

“以贫僧之见，那些傢伙的洞穴不可能只有一个。”

蜥蜴僧侣闭起大嘴。尾巴敲打着地面，竖起他那被鳞片覆盖着的手指。

“四面八方估计都有吧，从一个洞反着走到底就应该可以找到。”

“可是啊。”长枪手一边警戒着外面，一边说道。“那你知道本道是哪一条吗?”

“不管怎麼说，裡面大概都是互相连通的吧。”

关於地下的事情，没有人能比矿人更了解。

矿人道士把别在腰带上的酒瓶凑到嘴边一饮而尽，打了个酒嗝。

“应该用一个洞挖过来，要袭击前再岔开的吧，不然的话也实在要花费太多时间了吧。”

“那就决定了。从最靠近的洞裡潜入。没问题吧，哥布林杀手。”

“没关係。”

“那、问题、就是、那些孩子们、呢。”

魔女颇有意味地，向新手们那边瞥了一眼。

“还有、其他的、不是吗？那些雏鸟、要怎麼办……”

“是留下、还是带着、还是、让他们逃走？──……”

然後，长枪手突然一边嘻笑着，一边戳了戳重战士的肩膀。

“在洞穴裡可挥不了那把大剑吧?”

“是我大意了。”

过去的失态被翻了出来，重战士脸狰狞地扭曲了一下。

“嘛，总而言之比起下去我在上面更好一点。小鬼就交给我，洞穴裡面就拜托你们了。”

“噢”，长枪手喏了一声，“没问题”，哥布林杀手这麼回道。

老手们一瞬之间就商量好了计划。虽说已经算是比新人有稍微多迈出一步，但这到底也并没有女神官能够插手的餘地。不是像不在场的妖精弓手那样“不去做”，而是“做不到”。

倒不如说就算是那个森人，从外部把自己掺和到裡面的这种事也是有一定限度的。

有各种各样的观点、意见才会發生议论。反驳和对案，也不完全是否定其他观点的东西。然後对现在的女神官来说，观点──这种基於经验的东西，她压倒性的欠缺。

但也不可能因为这样就站在那裡无所事事，她也一直紧张地注意着外面的情况。

可是──。

是怎麼回事呢，这种无法言喻的不安。每每到这种时候就会如此，或许这也是一种啟示。

就像是在第一次冒险挑战洞穴的时候，她内心深处涌上来的不安。小小的胸中翻来覆去着的，正体不明的焦虑感──不能什麼都不做。不能这样下去。必须得做点什麼不可。

──可是，是什麼?

“啊。”

突然想到了那个可能性，女神官的嘴裡不由自主地漏出了声音。一瞬间，其他的冒险家们的视线都刺了过来，她的脸颊也微微发红起来。

“怎麼了。”

然後，最先出声的是哥布林杀手。

“哥布林吗。”

“……那，那个!”

声音上扬。更加引人注目。只是这样女神官就简直想逃走。

“其他的新人冒险家都已经回去了吧?”

“啊啊”，长枪手点了点头。“除了想练习夜战的那种人以外，基本到了傍晚就都走了吧。”

“那他们现在……在哪裡呢?”

“你想说什麼?”

以锐利的目光瞪着她的，是重战士。

也许他并没有在威胁的意思吧。但是，这种情况下。不管怎样的想法和情报都不会放过的严肃感，自然而然就会成为一种威压。

“那个，那个……”

女神官畏缩着。

说到底自己的意见有没有这个价值呢？会不会就只是个单纯的想法呢？说起来自己什麼的──……。

“说说看。”

哥布林杀手的，低沉的、粗鲁的声音。

无论什麼时候都不会改变的那个声音，女神官吞了一口唾沫。像是要压下颤抖的双手一般，紧紧地握住锡杖。

吸气，吐气。

“……大概哥布林的目标，是还在回去的路上的……新人们也说不定，我是这麼想的。”

“居然……?”

不知不觉地声音变粗的重战士。身上的装备一下發出声响，女神官一瞬间缩了一下身子。但是不能停下。也不能放弃。

“因为，很奇怪。哥布林，就我所知，应该是胆小怕事的怪物。”

──因为我就是这麼被告诉的。

要站在哥布林的立场去思考。哥布林的生态。哥布林的恐惧。

“如果我是哥布林的话，应该绝对不想攻击聚着实力高强的冒险者的地方吧。”

然後，你的大军才应该当作诱饵──……。

那是什麼时候呢。是和哥布林王对决时，他说的台词。

她还不成熟。经验不足。但是，也并不是完全没有。只是她自己没有注意到而已。

“……不是主要目标吗?”

哥布林杀手低声喃喃道。

“是想漏了啊。”

“所以，我提议……”

第一句话说出来，之後就比较轻鬆了。一边考虑着一边出声难免不会磕磕碰碰。但是说出的话本身却不会停顿，所以也没有再迷惑的道理了。

“所以，我要去。”

尽管现在集众人的目光于一身，但女神官仍拼命地说着自己的主张。

“我冒险者的朋友，那个，有两个战士、一个圣职者、一个魔术师……”

屈指可数。新手战士，圃人少女。还有见习圣女，和红髮的少年。

“如果那边再加一个圣职者（我）的话，只是这样就会有很大的变化。”

得去帮忙。我想要去。

银等级的冒险家们，因为这句过於直率的话语而面面相觑。

“……时间、差不多……没有、了呢。”

看了看外面的样子的魔女，绽开魅惑般的笑容，喃喃地说着。

“我不知道这个女孩的实力。没有评论的资格。”

听了魔女的话之後，长枪手立刻举起双手表示无所谓。

“……也是啊。”

接着，重战士眯起眼睛，像是在评估一般瞪着女神官纤细的身躯。

“分散开来有可能会被各个击破，你实力够吗?”

“以贫僧看来，这未必不是一个好想法。”

深思熟虑地点着头的蜥蜴僧侣，转了一下眼球。向女神官闭起一只眼睛。

“即要讨伐敌人本阵，但也不能放弃新手，不失为一手妙计。”

“就当是升级的考试，不是正好吗!”

库库地笑着，矿人道士捋了捋长长的白鬚。

“怎麼样，啮切丸。也该让雏鸟离巢了吧?”

──哥布林杀手先生。

女神官，带着依赖的心情，注视着这个穿着髒污的男人。

想到第一次冒险以来，离开这个人去冒险，真的是第一次。能做到吗？自己究竟行不行呢？虽然不是自己一个人，但也还是要靠自己的力量。能和哥布林战鬥吗？

大家都说她能够做到。没有在场的妖精弓手也，一定是这样吧。那固然是非常高兴的事，但也不能奢求太多。

但是。

──这个人说不行的话。

那时候，会乾脆地沉默吧。因为不管谁来说，一定都不是最好的。

但是他的话不一样。

“能办到吗。”

“能……”

只问了一句话。

他的问题很短，很简单。总是那样。

可是──。

正因为如此，我才想回应蕴含在那裡面的情意。

不得不，必须要回应。

女神官把涌上喉头的话语咽下，咬住嘴唇，然後高声回答。

“能……办到！”

哥布林杀手就这样盯着她看了好一会。看不清那铁盔裡面，隐藏着的眼眸的颜色。也看不清他的表情。

“是吗。”

他缓缓地点了点头，做出了决断。

“那就定了。”

§

“喝啊!!”

“G R O B R !?”

在狭窄的洞穴中挥舞着的真银（Mithril）的枪芒，贯穿了小鬼的喉咙。长枪手将手上的长枪不断刺出，伴随着高亢的魔力轰鸣声让死亡之花在身边不停绽放。

一挥一死，四击四杀。

就算哥布林举起各式各样的如盾牌之类的木板，他也视作无物。在狭窄的地方不能使用长枪本就是外行的想法，倒不如可以说枪真是一把万能的武器。

横扫，竖切，砍倒，突刺。连续突刺之後回身反拉，然後再出其不意地一刺。长枪不断刺出，无数次的攻击汇聚到一起，形成一股压倒一切的气势。被强化（Plus）过的长枪，如刮起的旋风一般到处肆虐，小鬼的脑浆和鲜血，不断飞溅到土壁上。

即使是在如下坡一般的洞穴裡的立足点，熟练的战士也依旧可以如履平地。

“别让它们逃走了！”

“裡面有六只──不，三只！”

在长枪手如夸示一般蹂躏着小鬼们的瞬间，木芽箭从长枪手的正旁边飞掠而过。妖精弓手如魔法般接连不断地引弓而射，三支箭不偏不倚地射穿在躲在洞穴深处的三只哥布林的眼窝。

“G R O R R B !?”

“G R O B ! G R O O R B !!”

剩下的不是六只，而是三只。只是单纯的减法罢了。如果不是确信自己能中，就绝不会如此射击吧。

“一……”

霎那间，哥布林杀手已经猛冲过去。

他疾奔着，手上的剑早已脱手而出，扎进了一只小鬼的喉咙。

“G R R R R O !?”

无视拼命挣扎着溺毙的小鬼，他从眼睛裡插着箭矢的小鬼的尸骸那裡“借过”一把短剑。向因为四个同伴一瞬之间的毙命而陷入混乱的哥布林的喉头，横向一挥。

他把宛如笛声咻地喷洒着鲜血的小鬼用盾打倒，然後回身一掷。或许是由於太过勉强的投法，飞出去的短剑偏了幾分，刺进哥布林的肩头。

“G O R B !!”

“三，只。”

哥布林杀手是不慌不忙地，从被喉头不断冒出的血所呛死的哥布林那裡夺过手斧。然後把它砍进最後一只哥布林的头颅，这场遭遇战就此结束。

杀死十只小鬼，对老练的冒险者团队（Party）来说，不过是小菜一碟。

气息丝毫不乱的长枪手扛起枪，就像是要说“你这傢伙”似的傻眼地盯着哥布林杀手。

“别给我这样随随便便地把武器给扔出去啊，太浪费了吧。”

“因为这是消耗品。”

“去找那种会回到手边的魔法投剑（Throwing Dagger）啊，应该有在卖的吧?”

“那个哥布林也可以用吧。”哥布林杀手说道，“被夺走怎麼办。”

“喂喂，没有时间了，快点帮我回收箭矢啊！”

对着一脸索然的长枪手，和从哥布林手中夺走武器的哥布林杀手，妖精弓手这样吼着。

三人虽然看起来都一副悠闲的样子，但动作并没有放慢下来。在一边不懈怠地警戒周围的同时，检视着自己的武具，準备下一次战鬥。

哥布林杀手低声沉吟了一下，地上除了一些被用的破破烂烂的东西之外，已经没有其他的武器了。理所当然，小鬼们自然不会怎麼爱惜装备，它们本就是掠夺种族。

“呀哈!”

蜥蜴僧侣看着那副光景，用力地点着头。

“有这两位前卫在的话，可真是高枕无憂啊。”

“你，平时都是站在前排的吧。”

“是、呢。”魔女，像是在感慨般一字一句地说道，“彼此彼此、呢，战士……都只有一个，对吧?”

把训练场的新人们都托付给重战士，从某个角落裡的洞穴潜入地下的冒险家们。与平时分为五人和两人两组的团队（Party）不同，这次是六人一组。

因此，也和平时的队列也会有变化。前卫是哥布林杀手和长枪手，还有妖精弓手，咒术使作为後卫。

妖精弓手的箭，和蜥蜴僧侣的法术，哪一个更重要？答案很明显。

“拔出来了。”

“啊啊，真是，箭头缺了个口了啦。”

她看了看从哥布林杀手那裡收到的箭，咂了一下嘴。但妖精弓手还是恨恨的把木芽箭放进了箭筒。这也是没有办法的事。

“欧尔克博格呢，有什麼好的武器吗？”

“没有什麼选择的餘地。”

“话说，为什麼没有问过我就决定让那个孩子去了呢?”

“不满吗。”

“才不是呢。”上森人移开视线。“你不担心吗?”

“担心，她能不能做好。”

真是的……然後这时，叹了一口气的妖精弓手的长耳朵一晃，一晃地摇动着。

“来了哦。”

“方向和数量？”

哥布林杀手一边快速地问道，一边从腰上的杂物袋裡取出一个皮袋。那是他的钱包，裡面有几枚硬币。上面缝着花的刺绣，像是个相当古旧的东西了。

哥布林杀手将钱包口紧紧地拉起来，發出了尖锐的响声。

“我不知道……有很多回声……”

“没时间犹豫了哦！”长枪手把枪尖上沾着的血脂拭去，一边吼着。“不管那条路，总不可能从上面来吧！”

“没办法了呐。……干吧。”

如此一来，不愧是老练的冒险者，反应速度很快。

矿人道士一边嘴上发着牢骚，一边把手伸进装着触媒的挎包裡，準备好法术。魔女慢慢地架起法杖，调整着气息準备咏唱咒文，蜥蜴僧侣则是双手合掌。

“真是的，所谓的小鬼治退，就是这种麻烦至极的东西啊。”

“真的，呢。”

轻轻笑了一下，蕴含真力的言语从魔女艳丽的嘴唇裡吐出。

“〈萨吉塔（箭矢）……萨伊努斯（弯曲）……奥法罗（赋予）〉”

魔法师的咒语是可以篡改世界之理的话语。团队（Party）里的所有成员，都被一股不可思议的流动所保护了起来，然後妖精弓手和长枪手一起喊出声来。

“来了，来了……两边！”

“快退下！”

沙土从两侧向冒险者袭来，与此同时，他们一起飞身退後。

“G R O R B ! G R R O O R O R B !!”

“G R O R O O B R R !”

所谓声势浩大指的就是这种事吧。（原文：雲霞の如く，意为人山人海，军势浩大。）

正常来说，冒险者生涯裡能看到的哥布林大多都是十只或二十只。现在却有远超这个数量的哥布林，为了一举击溃他们而破墙而出。

如野兽般的哥布林们的喊叫声。意思非常明显。屠杀、掠夺、应报。这是为同伴报仇。把冒险者们千刀万剐也不足惜。男人就毫不留情地斩断撕裂，女人就蹂躏侮辱一番之後也毫不留情地杀掉。

那个女人的话，要把魔杖抢走，把腿给绑住，甚至可以把她作为下崽皮囊用到死为止。森人的肉比较柔软，很好吃，要慢慢享用。从手脚的一端开始一点点切下来吃吧。

就算是哭叫着请求原谅，也绝不容赦。就像那些傢伙对我们做的一样，杀了那些傢伙！

“〈畅饮吧歌唱吧，美酒的精灵哦（Spirit），让人做个唱歌跳舞睡觉痛饮的好梦吧〉”

应该就会有幾只小鬼，被囚禁在那个美梦裡结束了一生吧。

矿人道士将口中的酒精转化为雾气喷出，从而发动了《酩酊（Drunk）》。随着先头的幾只哥布林啪嗒啪嗒地无力倒下，哥布林们就像是多米诺骨牌一样陆续倒下。同时後面的哥布林强行向前冲去，有好幾只哥布林被生生践踏而死。

惨叫、悲鸣，如阿鼻地狱般的景象。

“愚蠢。”

哥布林杀手毫不犹豫地挥舞着革制钱包，向附近的哥布林打去。装在皮袋裡的硬币经过离心力的加速，小鬼头骨这种程度的硬度的话可以毫不费力地轻易打碎。

用着苦寒之地的人们拼命赚来的钱币将小鬼打死，在某种意义上也是一种因果报应吧。

“G R B !?”

“G R O V E !?”

从眼窝中打碎眼球，然後从侧头部直接击烂大脑。

打死了一两只就可以了。哥布林杀手立刻就把一只哥布林踢倒，从其腰上的剑鞘裡抽出剑来。

“……呣。”

在那一瞬间，一只哥布林挥舞着毒短剑飞扑过来，用盾牌接下这一击，弹开。接连不断射过来的箭，都被不可视的力场所挡了下来，所以无视。也不成问题。

“到那边去了哦！”

“喂，不要给我增加工作啊！”

虽然发着牢骚，但他的本领确实十分高强。

长枪手把正面的幾只哥布林用枪一起贯穿。顺着拔出来的动作用枪的另一头向背後挥去。哥布林连人带盾牌一起被打倒在地，哥布林挣扎着想要起身，又是一击挥来，脖子被打折，就此毙命。

“这样一来背後就不会有哪怕一只哥布林了！”

“最开始就该这样。”

背靠背的两名战士，从正面迎击如浊流一般冲过来的哥布林。

从抢眼程度和强度来看，长枪手的力量明显超过哥布林杀手。他一挥动长枪，哥布林就像是割草一般的被屠戮倒下。

哥布林杀手的动作都必须止于长枪手的背後。清除长枪手漏掉的敌人，击倒自己正面的敌人，然後把自己漏掉的敌人交给背後。

飞来的石块就交给《避箭（Deflect Missile）》弹开，不去考虑防御。他们心无旁骛地继续挥着武器。

但，当然对哥布林来说，也不可能就只有这点手段。

“萨满！”

妖精弓手的呼喊一瞬间停下，在小鬼们後方的拿着法杖的哥布林吐出咒语。在那法杖的前段，魔法之光转眼之间就膨胀起来，然後突然飞散开倾注了下来。

这是一切攻击法术基础中的基础──《魔法箭（Magic Arrow）》。

它的威力虽弱但是必中，在这种乱战中会造成一定威胁。另外，因为这是法术一类的攻击，所以《避箭》无法防住。

小鬼的确有点小聪明。然而，长枪手却兴冲冲地喊了起来。

“交给我啦!”

“〈玛古纳（魔法）……雷莫拉（阻碍）……勒斯特吉托尔（消失）〉！”

和无奈地微笑着的魔女极为相仿的吟咏。

与哥布林萨满的蕴含真力的咒语互相拮抗的，正是《驱魔（Counter Magic）》法术。有魔法箭雨触到蕴含着魔力的话语，大多都如雾般散去，但还是有极少数碰擦到了长枪手。

“工作、不是增加、了……吗。”

“这就是你的工作吧！”

回应魔女的调侃的果然还是调侃。

血从长枪手脸颊的伤口上滴落，但这不是什麼大问题，他仍是向哥布林群裡冲杀过去。

“真是，如果要箭的话，这边也给你吃一记……!”

吐着恶言的妖精弓手，再次把蜘蛛丝作成的弓弦紧紧拉满，将箭射出。把洞穴裡面的尘土和湿气一併撕裂开来破空而飞的木芽箭，精准地射穿了萨满的喉头。

“嘁！”

“受伤了吗！”

在後方守着的蜥蜴僧侣，心焦地用尾巴拍打着地面大声喊到。

女神官不在的这个时候，他是唯一的圣职者，也是唯一有治癒奇迹的人。决不能莽撞地使用法术，但不能上前战鬥，对他来说似乎有些不合本意。

“没问题。”

哥布林杀手简短地回应，确认着自己的全身。

将粗糙的皮革铠甲连同锁子甲一同贯穿的伤口、血渍、以及疼痛。

──有痛觉，也就是说，他还活着。

他一边把剑往眼前的哥布林挥去，一边凭着绳结的触感在杂物袋裡摸索着。然後一把抽出药水一口气喝了下去，把那个小瓶子用左手投掷了出去。

“G R O O R B !!”

“死吧。”

对着由於突然的冲击向後反仰的小鬼，哥布林杀手毫不留情地把剑刺向它的喉咙。他将吐着血泡气绝身亡的哥布林踢倒，拔出剑来挥干血迹。

“法术还留着吗?”

“托、您的、福。”

对着吐着气调整呼吸的哥布林杀手的这个问题，魔女和颜悦色地回到。

“我也是。”

“要用《龙牙兵（Dragon Tooth Warrier）》吗?”

“不……”

对於同伴们的回应，哥布林杀手思索着摇了摇头。他低低地沉吟，盯向被小鬼们挖出来的坑道顶端。

妖精弓手，像是放弃了似的叹了一口气。

“……欧尔克伯格。又在想什麼不好的事情了是吧?”

“啊啊”

哥布林杀手点了点头。

“对哥布林来说。”

§

随着战鬥声响的逐渐远去，集结在仍在建设中的办事处的冒险者们顿时松了一口气。

“……到那边，去了吗?”

“好像是这样。”

也许能得救。也许能活下去。爸爸。妈妈。他们面面相对，用着幾乎要哭出来似的语调互相私语。

──这可不行啊。

一边在门口看着外面的情况，重战士深深地叹了一口气。

眼看着就要被压垮了啊。

嘛，也不是不明白他们的心情就是了。失败的时候。不幸的时候。谁都会害怕起来，变得犹豫不决。而且比这更重要的是，不想被哥布林这种的怪物所杀。谁都是这样。

但如果不去冒险又算什麼什麼冒险者呢？

纵然身陷囹圄，但仍至死不渝的，这才是真正的冒险者。剩下的，就看众神掷出的骰子能否使其起死回生（Critical）了。

然後────……。

咚，咚，咚。

伴随着地面的震动，沉重的脚步声响起。一下吓的浑身发抖的新人，胆怯地吞了口气，口中不自觉地漏出声音。

黑色的影子。

那个影子拖着笨重的身体前行，手裡握着一根巨大的棍棒。根本无需在脑子裡多考虑怪物的知识，重战士马上就知道了它的真面目。

“来了吗，鄕巴佬（Hob）。”

霍布──霍布哥布林。

出现返祖现象的小鬼的上位种。这是一种既无智慧又无武技，而仅仅是空有力量的个体。在大部分的巢穴中，它们都是首领或是作为保镖的强敌。

“喂，小鬼们，给我好好看着！”

重战士往手掌上吐了口唾沫，摩挲着大剑剑柄上的皮革，然後紧紧地握住。

“我不知道他们教了什麼，但是我要对你们说的话只有一句。”

这麼说着，重战士不紧不慢地从门口向外走去。

“H H O O O O R R B !!”

一步。二步。三步。朝着巨汉哥布林迈步前进。

只是哥布林而已。虽然不过是哥布林而已。

虽说无法与过去对峙过的小鬼英雄（Goblin Hero）相比。但是，以那个肌肉发达的臂膀挥出的一击应该也相当有份量吧，根據命中的地方会死也说不定。

“不管什麼怪物，只要对方有实体，的话！”

仅以双臂的力量来挥舞着这把巨大的武器，这样的事情根本无法想象。

向前踏出一步。以全身的移动为势。这是没有足够的力量就不可能做到的绝技。他扭过身体。

钢铁制的双手剑。其所花费的金额远非其他装备可比。价值完全不同。

将其──

“就算是神，也杀给你看!!”

──挥去，砍断。

§

小鬼们的脑子裡貌似只装入了奸计恶谋。

虽然这只是童话故事什麼的裡面所描述的东西，很少能切身感受到。

“G R O B ! G R O O R B !!”

“G O R R O O R R !!”

但为什麼会变成这样呢？

一边跑着，崭新的还留有些许硬实的皮革铠甲發出与内衬相互摩擦的声音，他一边拼命地思考着。手上本该拿着长剑，但怕是在跑的过程中掉了吧。每当他踏出脚步的时候，空剑鞘就不断打到腿發出啪嗒啪嗒的，像是在讽刺主人的愚蠢一般的响声。

在夜晚的黑暗中，小鬼的嘲笑声就像充斥与其中一般不绝于耳。在那因双月辉光而伸出的树木的可怕的影子中，有着如闪闪发光的星星似地燃烧着的瞳孔的哥布林群。这简直是一场如何努力睁眼也无法转醒的噩梦。

新人──那些早早结束训练的冒险者们，做梦也没有想到这副光景。

谁都是这样吧。

在想像困境的时候，大都会把自己想得能够飒爽轻鬆的脱身吧。也会想着自己即使在洞穴中被哥布林群所包围，也可以想出办法逆转情形，化险为夷吧……总之，这种被哥布林包围，在夜路之间被穷追不捨的情况，怕是万万没有想到吧。

“……可，恶啊!!”

“快点，这边!”

应和着不知是谁的喊叫声，他们竭尽全力地向森林方向冲去。比起在原野上被包围，不如逃向那裡或许还可以多一线生机。

最初是──十五人左右吧。

训练终了，缓缓走在黄昏的道路上，向着街道的方向归去。明天也是训练吧。但也差不多想去冒险了呢。叽叽喳喳地谈论着这些事情。

然後────

从人群最後面传来一阵惨叫。回头一看，一个少女被拖入黑暗之中。

──不然啊！不要，咿呀，啊，呜，啊啊啊啊啊……!?

那临终挣扎的声音，还残留在耳後。“妈妈……”她哭喊着，声音模糊不清。

当他猛冲过去，想要强行把人拽过来的时候，她已经断气了。一堆被撕得碎烂的破布，肉末，还有骨头，怎麼都已经不可能是活着的人了。

……之後就是一片混乱。

“是哥布林！”

虽然也有人喊着跑着逃走，但却一个人，一个人地分开消失……。现在剩下的只有五六个人了。

“哥布林什麼的，不是应该都待在洞穴裡吗?”

“但实际上就在这裡，没办法吧！总而言之，先到街上再──”

是因为太热吗，跑在旁边的战士早已把头盔给扔掉了。

所以才没能把他的话听到最後。因为从头上掉下来的石头，狠狠地击碎了他的头。

“什，么……！？”

──上面！？

把飞溅到脸颊上的脑浆慌慌张张地拭去，向树上抬头，他看到了。像是燃烧着般的哥布林的眼睛。

“能爬树什麼的，完全不知道啊！？”

只是没有哭出来这点就已经算是十分不错了。

还不满十五岁。在村裡是腕力最强的。只是因为这样就离开了故乡。挥剑的方法、探索的方法、野营的方法，还有很多其他的东西。只是知道了个大概，就觉得“自己已经明白了”，等到注意到也已经晚了。

倖存的五名冒险者们，强行压下不停颤抖着的膝盖，聚到一起。握着武器的手根本无法握紧，咏唱着咒文的舌头止不住的发颤，想要祈祷，但因为实在过於恐惧没办法集中精神。

哥布林们也因此再次發出了卑劣的嘲笑声。

“G O O R O R B !!”

“G R O O R B ! G R O R B !!”

用手指着害怕地颤抖的冒险者们，慢慢地迫近过来的小鬼们不知为何吵嚷起来。如果那些冒险者们有掌握哥布林语言的能力的话，肯定就会一下子陷入恐慌的吧。

胳膊的话两分、脚是三分、头算十分、身体就五分。如果是男人的话就没有额外的加分，女人的话就再追加十分。这实在是个好不恶毒的想法。反正都不知道是谁投了石头、扔了投枪，就连分数计算也没做，自然就会起争执吧。

哥布林们像是想到了一种有趣的遊戏一般，喜不自胜地拿起各自手上握着的武器。

已经到此为止了吗？

冒险者们只能一边止不住恐惧般的發出咯吱咯吱的牙齿轻碰的声音，一边死死盯着哥布林。然後，生锈的刀刃、带毒的枪尖、粗劣的石块，眼看着就要无情的挥下──

“〈慈悲为怀的地母神啊，请为在黑暗中迷途的我们，赐予神圣的光辉吧〉!”

就在那时，奇迹發生了。

灿烂夺目的如太阳一般的白光，伴随着强烈的压力袭向小鬼们。

“G O R O R O B !?”

“G O R R B ?”

在發出惨叫声而仰头弓背的小鬼们当中，有一两个影子飞了进来。

“喝呃呃呃呃!!”

“看招看招!!”

圃人少女是单手剑，新手战士则是来回挥着被其称为嗡嗡丸（Swing）的棍棒的。虽然稚拙，但仍是使出浑身力气的猛击（Bash）、猛击（Bash）、双手强击（Bash）。

像是刮起了大旋风一般，猛烈地向哥布林们袭来。

“G O R B !?”

“G O R O O R R B !?”

两人虽然都不能一刀两断，但只要从肩头把骨头和肉砍断，刀刃深达躯幹半身，敌人就会死。以小鬼为对手，根本不需要發出致命一击（Critical Hit）。

“呜，呜，果然我还是不习惯这个手感啊!”

“啊啊，又来了！”

大声斥责着拔不出砍入哥布林身体的剑的少女，新手战士一边把小鬼的尸体给踢飞。

这是看着哥布林杀手模仿的动作。如果是他的话，这时应该会放开剑，夺取武器吧。或者是长枪手的话，应该会更準确地抓住要害，如行云流水般的突刺吧。如果是重战士的话，那大剑的猛然一击，要撕碎小鬼简直轻而易举。

──可不能只是模仿那些人啊……!

正因为有这高远的目标，新手战士的鬥志才会被点燃吧。

“噢啦，尽管来吧……”

“真是的，如果你再把武器弄丢了，买替换武器要从你的零花钱裡扣哦！”

见习圣女高声地向新手战士呼喊着，然後迅速向冒险者们的方向跑去。

“喂，受伤的人！来这裡，我来帮你们包扎！奇迹只能对重伤的人用!”

见习圣女卷起法衣的下摆，急急忙忙地来回奔走，有幾名冒险者像是找到救命稻草一般聚集到她身边。大致看了一下，没有出现危急的伤者。也没有中毒的人。

能赶上真是太好了──这样的想法、倒不如说根本不可能这样考虑。她早已在赶来的路上看到了那十个人凄惨无比的尸体。

见习圣女咬住嘴唇，从包裡取出绷带。可没有对所有人都使用《小愈（Heal）》的餘裕。

“你，你们是……”

“我们是来帮你们的!”

發出尖锐的声音的是，高高举着闪着《圣光（Holy Light）》光辉的锡杖的女神官。鹅蛋脸上渗着汗水，直直地瞪着小鬼群，以坚定不移的信仰保持着奇迹。

“快点，快点到平原上去！到狭窄的地方正中哥布林下怀。”

“但，但是，要是在平原上被包围了……”

“我会用《圣壁（Protection）》守护你们的……快！”

女神官一边这麼喊着，一边冷静地思考着自己的奇迹的使用次数。

恐怕，到时应付哥布林猛烈的追击的逃亡中，要在那途中要再进行一次祈祷也没有什麼可能性。直到现在她还能祈求三次奇迹。这样的浪费无疑是致命的。

──也就是说，这次也不可能使用《小愈（Heal）》了吧。

“────”

虽然也颇有点想做助人治伤的事，但这就是自己的战鬥方式。正因为如此深信着，慈悲为怀的地母神才会为她带来更多的光明。

然後在为了救援而赶来的冒险者中只有一个沉默着的人，是红髮的少年。

剑戟声。二个前卫的气息。小鬼的悲鸣。两名神职人员的指挥声。冒险者们的呼应。少年一边望着一切，一边紧握着法杖，手指泛白，噤声不语。

因为在这五个人的团队（Party）中，具备最有威力的攻击手段的就是他。

──决不能再愚蠢随便的念咒文了。

像上次这样的失败绝不可以再来第二次。哥布林的数量很多。正常来说对包括自己在内的三个人来说，对手至少有十只以上。

那麼用《火球（Fireball）》一网打尽？不，不可能。敌人太过分散。一发要将複数个体卷进来是不可能的。只对一只小鬼用一发法术，太划不来了。

但也没有时间烦恼了。哥布林的数量太多了。当然要阻止也可以。就像那个成为俘虏的侍祭一样。但周围的女人们又会以什麼样的目光看自己呢。姐姐又怎麼瞑目呢──……。

少年魔法师的视野突然一片火热，像是燃烧起来一般，他拼命地使自己冷静下来。

那个奇怪的冒险者，哥布林杀手最为令人恼火的地方，就是冷静沉着。如果在这裡任由怒气發出法术的话，这样自己就会输给那个冒险者了。不，哥布林杀手什麼都不会说的吧。但是他决不允许自己这麼做。

──那麼，怎麼办?

只会投出火球闪电的人就不是真正的魔法师。

那麼，怎麼办──……?

就在这时，他的脑海裡电光一闪。

“大家，捂起耳朵！!”

“喂，等，喂，现在正在战鬥中啊……!”

“赶快!”

“啊啊啊，真是的!”

突然的指示令新手战士和圃人少女發出了悲鸣，但不可否认的是，已经没有时间可以浪费了。

红髮的少年看了女神官一眼，她也带着认真的表情点头。

“交给，你了！”

就像祭典之後的战鬥，还有在雪山的战鬥裡，自己被哥布林杀手所託付的一样。法术的分配，也正需要领队（Leader）的信赖与指示。

被託付了的少年──红髮的少年魔法师，点了点头，高举法杖。

“喂，你们也是，把耳朵塞起来，快点!”

在他背後，是见习圣女对被保护着的冒险者们的怒吼。新手战士和圃人少女也分别把眼前的小鬼砍倒，慌慌张张的拉开距离。

──良机，稍纵即逝。

少年的口中高吟着蕴含真力的言语，咏唱着咒语使其在世间显现。

“〈克雷斯肯特（膨胀）〉〈克雷斯肯特（膨胀）〉〈克雷斯肯特（膨胀）〉!”

重複的话语只有三句。不可视的力量卷起漩涡，满溢着漂浮在少年面前。

接下来發出的，只有一声怒吼。

“   呜 哇  啊  啊  啊 啊  ！！！”

大气撼动。

§

一触之间、一刀两断。

伴随着裂帛般的气势，重战士挥出大剑。

只一击，就把霍布哥布林的骨肉连同棍棒一同斩断，应该是猛击（Bash）吧。黑色的血沫一边惊人地飞溅而出，头被从中分成两半，巨汉的小鬼登时毙命。

看了一眼被吓得目瞪口呆的新人们，重战士将先前挥出的武具重新扛到肩上。

“嚯哦。”

震耳欲聋的咆哮，让周围一带都撼动轰鸣。

是谁的吼叫？从何处传来？

抬头望天，看起来也不像是那样──……。

“这不是很有气势吗?”

他这麼说着，凶猛地笑了起来。

§

在那一瞬间，哥布林杀手对那从天花板上飘落下来的土块，潮湿黏稠的触感作出了决断。

“上面。”

哥布林杀手把短枪捅进哥布林的喉咙。把那吐着血泡溺毙的哥布林尸体踢倒，然後丢掉手中的武器，从小鬼的腰带上夺过一把柴刀。

在枪的使用方法上，自己远不及长枪手精熟，哥布林杀手完全理解这点。

“向上开洞！”

对着哥布林杀手向後方的大声喊叫，从包裡拿出触媒的矿人道士如此回应到。

“又来？好吧，交给我了！!”

“洞？做那种事幹什么！？”

面对如怒涛滚滚般蜂拥而来的哥布林，长枪手一边刺出银枪一边怒吼。但细看之下他的全身上都布满了一些细微的伤口，昭示出他绝不是无极限的。

就算是多名熟练的冒险者上前线战鬥，也敌不过数量的暴力。即使只是轻微的疲劳和伤害（Damage），一旦要累积起来，该死的时候就会死。

“有办法。”

哥布林杀手简短地如此说着，用磨得十分尖锐的盾牌边缘把小鬼的额头无情切开。尽管如此小鬼却还没有完全断气，哥布林杀手随即又把刚夺过来的柴刀，狠狠挥下。

伴随着一阵不快的声音，脑浆飞溅到洞窟的墙壁上。

“在那之前，要让它们先感到害怕，往裡面逃。”

“难道要同时施展《恐惧（Fear）》和《隧道（Tunnel）》吗，这样的实在太难办到了！”

“小鬼杀手殿，只要赶到裡面去就行了对吧！”

矿人道士把扔到脚下的挎包当作垫子摸到天花板，开始刻起咒印。像是要护着他一样站到前方的蜥蜴僧侣，凶猛地露出獠牙。

是时候让保存至今的精神力发挥作用了。

蜥蜴僧侣用着不可思议的手势合掌、像是要以肺腑将空气抽乾一般深吸一口气。那简直就像是将要喷出吐息（Breath）的龙的準备动作一样。

“〈巨躯至伟的霸王龙（Tyrannosaurus）啊，君临于白垩之庭幔，请将您的威光赐予吾等〉!”

一瞬间，《龙吼（Dragon Roar）》在洞穴中释放出来。

蜥蜴僧侣嘶吼着，音波的吐息（Breath）撼动空气。是觉得是有令人生畏的龙在洞穴中咆哮着發出轰鸣声吗，哥布林们都打心底畏缩起来。

归根究底，哥布林就不是一个勇敢的种族。只有在自己處於优势，或是迁怒的时候，才会横冲直撞到处施暴。然後一旦遇上真正恐惧的事，它们马上就会胆怯涣散。

“G O R R R B B ! G B R O O B !!”

“G R O B ! G G R O B !!”

哥布林纷纷哭喊惨叫着，一边把手上的武器给丢下，开始往裡面拼命地逃走。像是要更进一步把它们撵走一般，魔女放出的《光明》也紧紧地飞在它们身後。

望着小鬼们的溃不成军的样子，蜥蜴僧侣哎呀哎呀地喷出鼻息。

“但是，它们想必很快就会回来，就算是龙的力量，也不会永远奏效呐。”

“无妨。”

短短地回应的哥布林杀手，毫不大意地弯下腰，凝视着小鬼们逃向的深渊。稍微有些疲惫的妖精弓手轻轻拍了拍了他的肩头。

“话说，欧尔克博格。你不会又想用卷轴了吧？”

“卷轴的话，只有一张了。”

“……完全不能放心啊。”

哥布林杀手一边看着矿人道士默默地在土壁上刻着术纹，一边点着头。

“这上面，是池塘。”

§

被魔法放大的少年的叫声，摇动着大气，响彻整片森林。

只是单纯的放大声音。相对于用蕴含真力的言语改变世间之理来说，这简直就是无不粗鲁的用法。如果在学院中这样用的话，自然是免不了一顿训斥了吧──但，现在不同。

虽然没有像《火球》一样的威力，但这巨大的音量绝对是压倒性的。就範围来说根本没有什麼东西可以与之相比。刚刚被这声音给近距离直击的小鬼要麼昏倒，要麼就是吓得愣在原地，禁不住逃走的也大有人在。

“G O O R O B !?”

“G R O O B !? G R R O !?”

少年握紧了魔杖，像是要把嘴唇咬出血一样，狠狠地盯着小鬼们的背影。

想杀光它们。

这样的随心所欲、发狂、杀戮。但自己却让它们逃跑了。

根本比不上。

姐姐的份、被杀的冒险者的份、那时候救出来的侍祭的份。大家所受的屈辱、绝望、悲哀、愤怒。这一切都化为无穷的怒意在腹中翻滚。

如果就这样让其尽情释放出来，该有多麼痛快啊！该有多好啊!

啊啊，但是、但是──……。

“快撤退！”

把少年回过神来的，是女神官拼命地大声喊出的指示。她挥着《圣光》还未消弥的锡杖，指向应该前往的方向。

“直接从森林出去，往街道上跑!”

“了解!”

最先回应的新手战士，把身边正躺着昏倒的哥林的喉咙一击打断，然後向前跑出。

“走吧，不管怎样，总之回去才是最優先的事！都跟过来!”

“前锋就让他来，我们跑在两旁警卫，後方就交给你了。”

“好好！”

被见习圣女这样喊到，圃人少女十分有精神地冲过去。明明战鬥了颇有一段时间，却没有一丝疲惫的痕迹，是因为种族吗？还是因为她的体质？

圃人少女朝人群最後跑去，从少年的正旁边擦过。

“幹的漂亮哦！这个，超厲害的！”

“……啊啊。”

擦肩而过，那浮现在脸上的自心而发的微笑。少年接受了那个微笑，在踌躇了一会儿之後，缓缓地点了点头。

在同伴们把冒险者们围在中间跑着的时候，他悄悄地越过肩头窥探着背後。

这次的法术不是杀伤性的法术。只是为了让自己逃跑的法术。对，这次的目的不是要消灭哥布林。而是救援，就是帮助他们顺利回到街道上。

如果能够把哥布林全杀光，自己的心情到底会多麼愉快啊。

但是──没错，但是啊。

──我，才不是哥布林杀手。

少年向前扭过头，和大家一起跑了起来。

再也没有回头。

§

如怒涛滚滚冲出来的哥布林，就像字面意思一样，被怒涛所淹没。从天顶喷出的池水变成泥沙浊流，倾注流向小鬼的坑道。

不幸的是这个巢穴是向下倾斜的构造。冒险者团队（Party）能躲到就算被飞沫稍微溅到一点也无所谓的地方，但逃到坑道裡面的小鬼们就……。

“G O R R R B B !?”

“G B B O R !? G O B B G !?”

小鬼们在泥水中淹死，挣扎沉浮的情景，只能用凄惨一词来形容。

“活该还真是活该，但是……”

长枪手用枪柄打在在水中挣扎的哥布林的脑袋上，将其彻底沉入水中，同时歪了歪头。

“这样就不能追击吧。水流走了之後还会再攻击过来不是吗?”

“在《隧道》的效果断了之前，使用可以造出冰的法术。”

对着像是不是很明白而摆起暧昧的表情的魔女，哥布林杀手马上發出下一步的指示。

“冻结住的话冰会膨胀，坑道就会这样直接崩塌…………这样入口就不能使用了。”

“嗯嗯，我、知道、了呢。”

“之後，就从地上找到巢穴，摧毁掉。”

哥布林杀手心中，早已有个大致成型的想法了。

哥布林们只是偷盗工具，并没有对食物出手。之前的哥布林治退也是一样。他们也只是用抓来的俘虏来打发时间。

如果是这样的话，它们的据点就不会很远。至于哥布林们看到了在建设的训练场和聚集着的冒险者，考虑了什麼……也就不得而知了。

“……那这就你来做吧。我可是很累了。”

长枪手费劲地抱着长枪，坐在坑道的角落。

“下次再叫我来什麼的，一定要是哥布林以外的冒险（Date）的时候哦!”

“我知道了。”

想到这幾个小时裡，谁都没有休息。彻夜战鬥、每个人都尘头垢面，想休息也十分正常。

这六名银等级的冒险者中体力最弱的妖精弓手，无精打采地垂下长耳朵。

“……累死了啊。”

“喂喂，也没这麼精疲力竭吧，说是还要去把那个巢穴给捣毁咯。”

“我知道啦，但……”

埋怨着倚靠在墙壁上的矿人道士，妖精弓手撅起嘴唇。

也不是说有什麼特别不满的地方啦。把沾到污泥的脸颊用力地擦了擦，然後倚在墙壁上的她这样嘟囔到。但这也是代表了在场大部分冒险者的心情吧。

“所以说，我会去的啦。”

在浊水中或沉或浮、被汹涌的水流卷走，这就是哥布林们的末路。

“不过，你还真知道这是在池塘下面啊。”

看着这一切的蜥蜴僧侣，像是丝毫不受影响地这样说道。

“小鬼杀手殿，可是有来过这一带吗?”

“啊啊。”

哥布林杀手，低头望向慢慢溺毙的小鬼们，无感情地说道。

“以前。一直都……已经是很久以前了。”

那天，有很多哥布林死了，然後，也有很多冒险者死了。

但是冒险者们获胜了。

训练场被守住了。

可是，哥布林的减少的迹象──从未有过。
