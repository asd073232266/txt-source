如果说是不犹豫的话，那是骗人的。

但是，我已经决定了，所以很快便说出了答案。

「我，要一起，要和首领们一起走。」

首领险峻的脸像是吃了一惊似得，表情有些惊讶。

也许是我的回答很让人感到意外吧。

「这样好麼......？」

我点了点头。

在人群深处，抚摸着山贼村民的背的光妈妈，用一副难以置信的脸摇摇晃晃的向我走来。

在我面前跪下，与我的视线平行地看着我。

「这样真的，真的好麼？」

「是的！不行么.....?」

如果被拒绝的话该怎麼办啊，光是想想就好可怕。有种体温嗖得一下就降下来的感觉。

「怎麼会呢！当然是很开心啊！」

光妈妈流着泪和鼻涕把我抱紧了。

被光妈妈紧紧抱着的我，认识到了光妈妈正因能和我在一起而感到开心，不知不觉的脸颊便鬆缓了。

「光妈妈开心的话......我也很开心」

然後，我也用双手抱着紧紧抱着我的光妈妈。

被谁拥抱着的感觉为什麼会这麼舒服呢，抱着光妈妈的手传来了温暖的体温和温柔的气息。

抱着我的光妈妈也是同样的感觉么。

我享受着这份温暖，光妈妈凑到了我的耳边。

「谢谢你......莉由酱，呜。&*^。呜@#！」

听到了这样含糊的话语。

虽然後面的东西听得不是很懂，但是，光妈妈正在因我感到高兴到哭泣，并紧紧的抱住了我。

单是这样，我就已经很满足了。

我看着抱着光妈妈的自己的手臂，一如既往的是正常的肤色。

还想着我会不会是兴奋了就会长出角皮肤变绿的种族呢，但是看起来并不是。

虽然一直想被某个人爱着，认为不被爱就没有活着的价值。但是，我只是爱着谁，便会感到幸福了。

其证据便是我早已喜欢上了山贼团的大家，并早已感受到了幸福。

因为说了天黑之时就要出發了，所以我把辣椒的幼苗交给了留在村内的村民三人。

也教给了他们防止野猪破坏田地的方法。

本来，这些东西是为了获得「厲害的莉由」这个称号的道具。应该由我对村民们进行教导的，一边观察经过，但是现在做不到了，便委托了ゴズル先生回去说明。

为了保护田地的话，可以吧辣椒种在野猪时常出没的地方，如果带来的分量不够的话可以去山上採集代替。若是还不够的话，可以将果实撒在附近来代替。这麼传达了。

虽然托肥料的福田中种出了幼苗，但是若是被野兽破坏的话也没有意义了吧。

ゴズル稍微有些半信半疑的样子，但是不试试也不知道效果，我便把这些事情教给了他。

然後，把辣椒的幼苗交给了他。并若有可能的话拜托了他帮忙向魔法使传达一些事情。

「我过得很好，所以没有问题。我已经不想再回雷恩·弗雷斯特的领地了。若是可以的话，请连山贼们也不要再追捕了。」

村民山贼（原）听了这些话之後露出了一副複杂的表情，不过，姑且还是听进去了。

然後，在太阳落下的同时，我们偷偷地不在村民的发现下离开了村子。

我像往常一样座在光妈妈的前面。

库尔马尔先生也好凯先生也好光妈妈和利威尔先生也好（就不能说是大家么。。）都没有精神，一副无精打采的样子。

接下来要去的地方预定为卢比法尔的领地。

出發前商量了一会接下来该去什麼地方，但是果然下一站还是要去找巴修先生。

商量好了要找到从农地巡回回来的巴修先生。

实际上，我们连巴修先生所在何方都不清楚。只能按照大体方向行进并向路人打听道路。

老实说，不知道要多久才能见到回来的巴修先生，可能要花一两年也说不定。

尽管如此，首领还是要去见巴修先生，好像有什麼不得不说的事情要找巴修先生。

渐渐远离了咕里咕里村。让人感到有些悲伤。咕里咕里村，对我来说，也是让人心情感到複杂的村子呐。。。

把我拐走安置的村子也好，决定要和山贼一起生活而背叛了的村子也好。当然，也不是村裡所有人都背叛了首领。

我想大多人都会认为比起生活在狩猎与掠夺这种不安定的生活中，还是在被魔法使守护下的生活比较好吧。

我也是，在卡里卡里村若是遇到同样的选择的话，我也会选择比较安定的生活吧。

但是，与那个不同。若是在此做出了这样的选择，或许会失去什麼重要的东西吧。

虽然發生了各种各样的事情，但是我并不讨厌这个村子，或许是因为这个村子很像卡里卡里村吧。

看了看前方，首领的背影还是让人感到有些无精打采。

很辛苦吧。首领把村民山贼三人都当做弟弟那样看待。而且一开始旅行时，大家也都非常仰慕首领的样子。

没想到会有一天要从这个村子裡逃跑。

拿起精神吧，首领。之後我会好好抚摸着你的背的。（安慰的意味吧）