连续幾天来，我一边帮忙治疗受伤的人们，一边与村民一起生活在洞窟，不，是生活在圣窟裡。
在圣窟裡，有一块通风良好的地方用来生火煮饭。这裡是一个在生活方面毫无问题的设施，做的真是不错啊。
如果不是有一个叫做圣窟的奇怪名字的话，这裡就是最棒的避难所了。

「姐姐！有什麼要帮忙的吗？」
看到我在煎药，作为尤莉教虔诚信徒的小男孩，露出了一如往常的灿烂笑容过来询问我。
对待从领主那来的我和光妈妈，大人们似乎有所顾虑，但孩子们好像对我们非常感兴趣，经常跟我们说话。

「现在没问题」
「如果有什麼需要的话就告诉我！我会来帮忙！」
「非常谢谢」
斯鲁斯鲁村裡的好孩子非常多，经常会像这样过来想提供帮助。

就算是生活在昏暗的圣窟裡，但是只要能听到孩子们精神的声音，就会让人不由得心情舒畅。
「呐、吶！姐姐是从很远的地方来的吗？有见过尤莉大人的样子吗？」
正当我沉浸在天真无邪的小孩子那暖烘烘的气氛裡时，突然飞出一句对心臟不好的话，笑容不禁一阵痉挛。
小朋友，不行哟，那不是可以随便说出口的事哟。有那样的设定吧？
「……没见过呢，因为是迷之人物。而且，你看，关於那位的事，随便说出来，嘴巴会烂掉的吧？」
虽然我努力保持笑容对小男孩这麼说教，可对方却完全不在意。
「诶——？没见过吗？怎麼会——。我好想和尤莉大人见面啊——。据说去圣地的话就能见到尤莉大人。可是离这裡太远了」
「圣地？」
「嗯！是叫卡里卡里村的地方。尤莉大人就是在那个村庄的蒲公英裡出生的！」
圣地卡里卡里村。圣地卡里卡里村。圣地……。
我不由地把视线从孩子们身上移开……。因为，那个圣地……。

「是，是那样吗。可是见到尤莉大人的话，那个，眼睛不会溃烂吗？……因为太神圣」
我总算挤出了一句话。
「嗯！但是我是好孩子，就算直接看到，眼睛也不会烂掉！只要作为一个好孩子，每天都有祈祷，给妈妈帮忙，一直有在努力学习的话，就没关係。所以，我做了一堆好事，绝对要去卡里卡里村！」
难道说，村子裡的孩子都想抢先过来帮忙，就是为了平安见到尤莉大人？还有，卡里卡里村前面被加上了『圣地』两个字，总让我有种很微妙的感觉……。
卡里卡里村，现在究竟变成什麼样了呢……。

「真是的，这孩子非常喜欢尤莉大人。「做坏事之後，坏孩子是见不到尤莉大人的哦，因为会被尤莉大人吃掉！」因为听到这样的话，所以他就觉得只要当个好孩子的话就能见到尤莉大人」
孩子的母亲走了过来，加入了对话，疼爱地笑着抚摸孩子的头。

是、是这样的吗。坏孩子会被吃掉呢……。
唔唔，尤莉终於变成像恶鬼一样的存在了……。

还是不要想太多。多亏了尤莉，这个村子才能避免最糟糕的情况，受伤的人们也能进行应急处理。
真是幸运呢，幸运！

就在我说服自己幸运就是幸运时，圣窟外面突然骚动起来。
我急忙赶到圣窟出口，就看到有一个受伤的男人，在其他村民的搀扶下走回圣窟。

「發生什麼事了？」

我跑到那边询问後，对方痛苦地开口说道。
「巨、巨大的魔物……在村子裡……」
说完就失去了意识。
我在前面撑住倒下的村民。感觉非常的沉重，不过还有呼吸……。
「光妈妈！」
「知道了！把那个人搬到深处去治疗。虽然伤口很多，不过都很浅，没问题」

这麼说完，光妈妈就从下方架起昏迷村民的肩膀，走进圣窟深处。

圣窟裡弥漫着一种吵闹且不安稳的气氛。

有大型魔物来到村子。
格洛丽亚夫人呢？现在应该正在进行结界的修復作业。是和过来的魔物错过了吗……。

虽然我还想向受伤回来村民打听一点情报……。可是他们还不是可以说话的状态。

总之，先去看看外面的情况吧……。

「不好了！洞窟附近出现了魔物！非常大！男性都拿上武器快出来！」

不会吧。
难道是偷偷跟在刚刚那些人後面。
我急忙冲出去查看情况。
数名男性已经拿起武器，保持着距离摆好了架势……不行啊，体型相差太大了。
那不是拿着铁锹的外行人可以对付的对手。

身形高达3米，和熊一样有着棕色皮毛的身体，上面却长着一个鸟头，长相非常恶心的魔物。
那只魔物完全不在意拿着农具突击的村民，淡定自若地前进，右腕轻轻一挥就打碎了突刺过去的农具。

「大家快离开！太危险了！」

在魔物準备再次挥动手腕击中村民前，我射出了箭矢。
狙击顺利射中了眼睛，魔物因疼痛不禁後仰身体。

「快点，趁现在！拉开距离！然後给我一匹马！」
听到我的怒喊，拿着农具的村民慌忙离开了魔物。

在那期间，我继续拉弓，接连放出箭矢。射向魔物的另一支眼睛，柔软的喉咙以及胸口……。
可是覆盖魔物全身的坚硬皮毛阻止了箭矢深入。而开始警戒的魔物也挥开了射向脸部的弓箭。

我不禁咋了下舌。
这时听到附近传来的马鸣声，村民好像已经将马牵过来了。
看到附近的魔物後，马似乎有些兴奋，我则轻抚头部让马儿平静下来，接着纵身跃上马背。

在这期间，魔物一直警戒地盯着我，随後轻颤身体，弹开了扎在身上的箭矢。
幹劲满满啊。
这裡人太多了，而且圣窟裡还有女人和小孩。
还是先把魔物引到远处比较好。

我驱马远离圣窟，魔物则紧追在我身後。
途中看到了光妈妈慌张地跑出圣窟裡的身影。
似乎被我做的事吓了一跳，看起来非常的担心。

啊啊，或许又要被骂了……。就算会那样，我现在也只能这麼做。

不过，我能使用治癒魔法，没问题。光妈妈也知道，一定能理解我。
我这麼说服自己後，把注意力重新移向魔物。魔物以四脚着地的状态径直追向我，而且速度比马儿还要快一点。
我试着在骑马的同时拉弓射箭。虽然不能给予多少伤害，但只要让魔物产生些许胆怯就行。我就这麼牵制着魔物，冲进森林深处。
被这样一直追下去，迟早会被追上。
要射出火箭吗。使用火柴的话……。

火柴就放在我的口袋裡。可是，在马上点燃火柴，并且放出火箭，这种事做得到吗。
因为是匆忙中离开圣窟，幾乎所有的秘密道具都没带在身上。
如果有酒或油的话，只要在箭头绑上布条再沾上助燃剂，大概就能射出火箭……。
可是现在手边只有艾伦制作的短剑，弑神之剑，还有放在口袋裡的火柴、烟玉以及用硬币削成的飞镖，最後就是手裡拿着的弓箭。另外，这匹马似乎是用来运货的马，所以在马鞍旁绑着一条坚固的绳子。
该怎麼办呢。
用箭矢和硬币手裡剑，恐怕没法对那傢伙造成伤害。

必须直接用剑攻击那个魔物……。
而且……一定要有足够的力量。能够穿透那些坚硬的皮毛，直接插进身体的力量。而且还要有躲避魔物攻击的敏捷与胆量……。

不过，那些要素，我现在都能简单入手。

我将绳子缠上艾伦给我的那把短剑的剑柄，绑成十字型。
同时咏唱咒文

「アフコトノタヘテシナクハナカナカニヒトヲモミヲモウラミザラマシ」（注）

随後身上开始散發出一种气场。身体在微弱光芒笼罩中，渐渐发热。感觉浑身充满了力量。
这个咒文是和光妈妈一起坐马车时发现的，能让人兴奋，引出力量。
起初，咏唱这个咒文时，能轻易压碎核桃壳。现在也是，感觉自己什麼事都能做到。

我甩起一头绑有短剑的绳索，投向前方一根较大的树枝。绳索挂到树枝後，短剑在惯性下带着绳子卷住树枝。我一蹬马背，抓紧绳索，像泰山一样荡在空中。
荡在空中的我利用摇摆的惯性，跳到正好冲过下方的魔物背上。
随後，将弑神的短剑深深刺入魔物的脖颈。

能感觉到刺入肉中的柔软手感。多亏了魔法带来的兴奋状态，我能轻易使出巨大的力量。
短剑深深地刺入魔物身体，比想象的还要容易。因为疼痛，魔物一下子仰起身体。
为了不被抖落，我握紧刺入魔物身体的短剑，另一支手则紧紧抓住魔物的皮毛。

当魔物再次落下，恢復成四肢着地的那一瞬间，我再次用力，将短剑横向劈出。

「嘎——」魔物發出了些许像是尖叫的声音，在魔法的作用下，力量沸腾，充满万能感的我用幾乎要斩首一样的气势挥出最後一击。
就像砍只有5张皮一样的感觉，轻易斩下魔物的首级，被斩下的鸟头在空中画出一条弧线後落到地上。

失去平衡的魔物，一下子失速，四肢扭在一起倒向地面。我为了不被甩向前面，紧紧抓住魔物的皮毛，将短剑插入魔物的背部。
魔物横倒到地面後，我放开紧握的双手，慢慢站起身。

被斩断首级的魔物，四肢还在不停抽搐，大概还没死透。
我拔出插在魔物背上的短剑。
以防万一，我再次咏唱激发力量的咒文，砍断魔物四肢的肌腱。这样，魔物就没法站起来了。

说起来，真希望在砍掉头部的时候就能不再动弹啊。

我呼吸急促地俯看魔物。回过神时，第二次的魔法效果似乎也到头了，已经看不到包裹在身体周围的气场。
不知是因为之前的兴奋状态而没有注意到，还是无法感觉到，现在身体裡非常疼。大概是因为超出界限的使用了肌肉吧。
脚也使不上力，只能蹲在地上。这次，我咏唱了治癒魔法。
全身再次被光芒包裹，我忍耐着一如既往的刺痛感，治癒起伤口。
感觉自己最近已经开始习惯这种疼痛了。
「莉尤酱！」

在我使用治癒魔法时，听到了光妈妈的声音。我抬起头，就看到脸色苍白的光妈妈骑着马，低头看着我。
一看就知道了。
光妈妈好像，非常的生气……。

——————————
完

（注：小仓百人一首第44首
逢ふことの絶えてしなくはなかなかに人をも身をも恨みざらまし

中译：
邂逅逢时心已动，
而今倾慕两难中。
人生若只如初见，
不必相思满畵栊。

PS：翻译版本众多，这边只是选取其中一种）