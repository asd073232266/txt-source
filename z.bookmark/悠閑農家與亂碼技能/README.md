# novel

- title: ゆるふわ農家の文字化けスキル　～異世界でカタログ通販やってます～
- title_zh: 悠閑農家與亂碼技能
- author: 白石　新
- illust:
- source: http://ncode.syosetu.com/n7352fa/
- cover:
- publisher: syosetu
- date: 2019-02-08T18:47:00+08:00
- status: 連載
- novel_status: 0x0000

## series

- name: ゆるふわ農家の文字化けスキル　～異世界でカタログ通販やってます～

## preface


```
美味いメシと酒。
そして綺麗な嫁達がいれば、そこが異世界でも良いじゃない。
そんな感じでオジサンが異世界に転移して、まったりと農業をやることにした。
日本の調味料で作った料理で嫁達も自分も大満足。普通に異世界の食材も美味いし、自分の農作物もめっちゃ美味い。
これはまるで独身男性が実家に帰った時のような感じの、ゆるふわな優しい時間を過ごす男の生活記録である。
```

## tags

- node-novel
- R15
- syosetu
- ご都合主義
- ざまぁもあるよ
- ゆるゆる
- スキル無双
- スローライフ
- チート
- ドラゴン
- ハーレム
- 主人公最強
- 優しい世界
- 剣と魔法
- 微エロ
- 日常
- 残酷な描写あり
- 異世界転生
- 異世界転移
- 苦戦なし
- 鬱展開なし

# contribute


# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- novel_id: n7352fa

## textlayout

- allow_lf2: true

# link

- [dip.jp](https://narou.nar.jp/search.php?text=n7352fa&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [悠闲农家与乱码技能吧](https://tieba.baidu.com/f?kw=%E6%82%A0%E9%97%B2%E5%86%9C%E5%AE%B6%E4%B8%8E%E4%B9%B1%E7%A0%81%E6%8A%80%E8%83%BD&ie=utf-8 "悠闲农家与乱码技能")



