

　――捨弃艾路菲斯扎库的話，我就能逃出去了。

这种想法充斥着头脑之中。

现在的我放不出可以打破土魔将那坚硬的装甲的攻击。
就算只是争取时间，也伴随着相当程度的风险
弄不好的話，一击就会失去性命了。

土魔将以猛烈的气势朝这边冲过来了。
思考的时间只剩下幾秒钟了。

本来提出共同战鬥就是艾路菲斯扎库。
和琉泽苏他们成为同伴的时候一样，我又顺着气氛，迫不得已地和她结下了合作关系。
对方是魔族，对这边会有什麼企图也说不定。

【对了】

艾路菲斯扎库是魔族。
是到现在为止一直都与之战鬥的，敌人。
背叛敌人有什麼错。
完全没有要帮助她的理由。

这也许是逃离这裡的唯一机会了。
这裡就算是背叛她，也必须要逃走。
因为对我来说，还有不得不去做的事。

眼前是艾路菲斯扎库的後背。
她现在还没有注意到这边的动作。

【——呲】（脚和地面摩擦的声音，鬼知道怎麼翻啊 (╯°□°)╯┻━┻）

以捨弃她的的打算，我向前迈出了脚步。
连续地發出“喳”的踢起沙子的声音。
艾路菲斯扎库也应该听到这个声音了。
他也应该注意到我想要逃走了吧。

明明是这样。

【————】

艾路菲斯扎库什麼都没有说。
只是相信着我，以无防备的姿态积蓄着魔力。

——因为你救了我呢，那和人类还是魔族是没关係的吧？

想起了她那句話。
这傢伙是真心，这样想的吗。
和人类还是魔族无关，明明是敌对的种族也还是相信着我的吗。

艾路菲斯扎库没有回头过来。
有她那种程度能力的話，明明是明白我是想要逃走的。

什麼都不说的那个身影——只是一直相信着我。

『オオオオオオ!!』

土魔将举起了手臂。
能轻易碾碎一个人的那手臂向着艾路菲斯扎库挥下。

【————】

注意到的时候。
我已经抱着艾路菲斯扎库跳走了。

【……伊织】

在我的怀裡，她露出了惊讶的表情。

挥下的手臂挖开了地面，把地板击碎了。
我躲开着碎片，和土魔将拉开了距离。

【不要这麼纠缠，快消失吧】

念着不祥的話语，土魔将咆哮道。

土魔将张大了口。
集中在那裡那庞大的魔力。
那恐怕能确实地杀死我们两个吧。

还能逃掉。
在那傢伙积蓄魔力的时候，还可以——。

【——只有那一击，我会挡下来给你看，所以快点，艾路菲斯扎库】

对，我说出口了。

啊啊，对了。

在这裡见死不救然後逃掉是不行的。
利用她是没问题的。
但是欺骗，背叛是不同的。

如果这样做的話，我就会变得像那些人渣一样。

【——啊啊！】（表示知道了）

看到强有力地点头的艾路菲斯扎库，我做好了觉悟。

◆

【虽然被你那小把戏阴到了，但那也已经结束了。就由我来超度你们！】

土魔将吼叫道。
以它嘴边集中的魔力量来看，如果那个被放出来的話，就做什麼都没用了。
并且现在的我没有能防住那个的手段。

——是的，现在的我的話。

取出来的是，放出虹光的迷宫核。
内含着的是，足以驱动迷宫的魔力。
把手中的迷宫核就那样握碎了。

【库。。。！！】

放出的魔力的奔流涌入了右手的证明中。
由於太过激烈，体内嘎吱嘎吱作响。
暴风雨一样呼啸着的魔力，令视野中一片噪音。（视野有噪音。。。。）

【——啊】

然後，我明白了。
不够啊――
迷宫核内含的魔力这种程度还是不足以啟动证明。

『然後我就向魔王大人报告吧。你这傢伙是不值一提，矮小的存在！！』

积蓄完魔力的土魔将的哄笑回响着。
术式已经完成了。
已经没有任何办法了。

【……玩笑了】

即使面对着那个，艾路菲斯扎库也没有动摇。
没有怀疑，没有不安，只是相信着我积蓄着魔力。

【——崩坏巨桩】（崩坏巨桩是我编的，原文招式名是崩杭，读音是崩坏桩。。。）

产生出来的是巨大的桩。（就像打桩机那种吧，大概）

那有着和土魔将的巨大身体相匹大小的桩像子弹一样发射了出来。
被那种东西直接命中的話，肯定一下就玩完了吧。

【别开，玩笑了。。。！】

偏偏在这种时候，证明完全没法啟动。
对使用不了的力量，更多的是没了它就什麼都做不到的自己，我感到无比的焦躁。

从口袋裡面裡面拿出了一次能使用的最大限度的魔石。
用要渗出血一样的力度紧紧握着，解放出了内含的魔力。
即使到界限为止使用了魔石，也不能使出能防下那桩的魔术。

【库。。。】

因为勉强地使用魔术，魔力就快要散开了。
压倒性的魔力不足。

【我可有着不得不做的事啊】

怎麼能在这种地方死去。

堵塞着证明的某些东西——强硬的突破它，引出其中的魔力。
存在的东西全部，即使如此还是不够的話，没有的部分也要强行弄到手。

【不要，碍事啊。。。！】

回响着轰鸣声，崩坏的桩子逼近了。（崩坏是招式名，大概）

紧握着的魔石扎进了手裡，血滴了下来。
右手像烧起来了一样的热。

【哦，哦哦哦哦哦哦哦！！】

应该不足的魔力。
可是，从体内深处溢出了好像在燃烧着一样的魔力，聚集在右手上。
举起像要完全烧焦的右手，我發出呐喊。

曾经的自己使用过的，最强的防御魔术。

【魔毁封杀————！】（读音是イル・アタラクシア，我没查到是什麼，有兴趣的可以查一下。）

在正面展开的是，能匹敌桩子一样大小的盾牌。
被放出的桩子和盾牌激烈地衝突，一边散开着冲击一边停下了前进。

『什麼……！？』

魔毁封杀。
绝对不会被毁（读作破坏）的盾牌——以这样的想法，我所创造出的防御魔术。
不只是物理攻击，就连打过来的魔术也能消灭掉，如同名字一般（把攻击）封杀掉的光之盾。

和崩坏之桩对抗着的无毁之盾。（前面土龙的技能名改为崩坏之桩）

因为是不完全地发动的那个开始摇晃，破裂。
我不能输。

【库——啊啊啊啊啊啊啊。。。！！】

大声疾呼，用力地踩着地面。
然後，在握着的魔石碎开的同时——

『……怎麼可能』

桩子和盾牌同时破碎掉了。
土魔将动摇的声音在房间裡回响着。

　……防下来了。

如果再差一点魔力的話，我的盾牌就输掉了。
是强行引出魔力的影响吗，全身都被倦怠感侵袭着。
右手的证明，像是燃烧似的发热。

『那样的話……!』
【库】

土魔将张开了口，打算放出岩石的炮弹。
已经用不了那个盾牌了。
只能孤注一掷，打算用宝剑挡开炮弹的轨道的时候。

【――做得不错，伊织。】

听到那凛然响彻的声音，土魔将定住了。
那个声音的主人那眼睛染上了真红色，暴风般的魔力缠绕在身上。

『呜。。。艾路菲斯扎库』

知道形势不利了吧，土魔将取消了岩石炮的发动。
就像最初出现时一样，那巨大的身体开始慢慢地沉入地面。

【——别想得逞！】

在那之前先行动的是艾路菲斯扎库。
那真红的眼睛睁大了。

『怎麼可能。。。居然被日和还有失败者，本大爷我——』

【——魔眼・灰烬爆——！！】

红莲的闪光奔走向大声疾呼的土魔将。
那令人目眩程度的爆炸捕捉到了正在沉入地面的土魔将。
和刚才同等的，像暴风雨一般能把岩石一样的装甲都吹飞的强烈的一击。

『嘎。。。啊』

爆烟散去后，土魔将的身姿显现了出来。
全身都被烧焦，因为爆炸一边的手臂都被消灭掉了。
确实的致命伤。
那是。

『我。。。还没有。。。输』
【。。。你这傢伙，居然！】

即使受到了那个攻击，土魔将仍然没有倒下。
明明魔力已经见底了，却仍然强行发动着治癒魔术，想要治癒创伤。
伤口以完全比不上当初的速度癒合着。

【库。。。现在的我也幾乎用光魔力了。已经，只留下一点点了。。。！】

用出了两次魔眼的艾路菲斯扎库皱起了眉头。
土魔将能再次行动的話，我们就无计可施了。
但是，都到了这种地步了怎麼能输。

【艾路菲斯扎库！残留的魔力能注入这把剑裡面吗？】
【你打算怎麼办？】
【就用这个给它最後一击】

她的魔力注入了递给她的剑中。

【对不起。。。这就是极限了】

注入了艾路菲斯扎库的魔力的剑上，染上了不祥的颜色，变得漆黑一片。
可怕程度的魔力。

【。。。不，足够了！】

踢向地面，我朝着土魔将跑了出去。
一边漏出苦闷的叫声，土魔将对接近的我挥下手臂。
迫不得已而放出的攻击瞄准得太天真了，马上就敲打到我的侧面。

【哦，哦哦哦哦！！】

跳上那个手臂。
在那被凹凸不平的岩石覆盖着的手臂上，我全力地跑了起来。

【就这样，结束吧】
『库——哦哦哦！！』

染上了黑色的宝剑把那装甲剥落了下来，向着那裸露出来的头部挥下。
刀刃斩开了土魔将的肉。

『无用。。。的！！』
【呜】

即使如此，也停止不了土魔将。
土魔将摇晃着头部，把我抛到空中去了。
宝剑还插在它的头部。

『去死吧，人类类类！！』

瞄准在空中的我，土魔将挥起了手臂。
现在我没有办法躲开。
但是。
紧接着，插着土魔将的剑放出了光芒。

『这是，什麼。。。！？』

在和土魔将的战鬥的时候，事先想出的一个方法。
岩窟龙的弱点之一——没有被岩石覆盖着的体内。
那样的話，在它体内放出珍藏的攻击就好了。

『难道是。。。你小子，你小子！！』
【——结束了，土魔将】

那把宝剑是，内部含有魔力的魔力付与品。
和魔石一样，让内部的魔力暴走的話，就能当做炸弹来使用。
魔剑本身就有足够的魔力量，在那之上再加上艾路菲斯扎库的魔力的話——

【坏魔——！】（原文是坏魔，然後读音是break magic直译是破坏魔法，我这裡是直接放原文了，大家觉得哪个好呢）

不祥的光炸裂开来。
暴走了的魔力的业火流入了土魔将的体内。

『————嗤！！』

像被刺破一样的临终。
紧接着，土魔将的头部爆裂开来，盛开了魔力的火焰。
就像要突刺到天花板一样，爆烟弥漫开来。

『————』

失去了头部的身体摇晃着。
像小山一样的身体倒了下来，迷宫也跟着摇动。
在那之後，土魔将就再也没有动了。
奈落迷宫的主人这次是完全地断气了

【。。。伊织！】

在即将落到地面之前，又被艾路菲斯扎库抱住了。（就是接住了男主。。。。。。慢着，难道是公主抱？233333）

这样的話，我们的立场又要反过来了。
是强行使用魔术的影响吗。
就像电池要没电一样，身体脱力了
在艾路菲斯扎库的手臂中，意识渐渐远去。

【——果然，你是】

在最後好像听到了这样的嘟哝。

