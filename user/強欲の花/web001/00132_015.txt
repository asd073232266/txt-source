第二天，与昨天相同的时间起床的我确认了自己的状况。


“……吖，还是有些痛不过没问题……只要请求丹（ダン）的话应该能把我送到医务室的吧？”


要是现在的话丹肯定是在训练的吧。城之中是有医务室的向丹请求的话应该能带我过去。


“那也得是自己的恢復力惊人才行啊。没想到吐血了还能走到……”


热血格闘漫画等对攻击腹部而吐血是有描写的，但所谓实际吐血是冲击的级别达到了对胃等消化器官像破了一样的那种危险的损坏才会發生的。幸好肋骨好像没有断裂否则说不定即使走路也走不了。（这个时候的我）幻想着各种各样的事。




虽然昨天也是那样不过这个时间女仆不在王城内是为什麼呢？


到了训练场看看和预想的一样丹做着空挥训练的样子。照旧挥洒着汗水有些闷热的感觉。


“呼……嗯？哦诚一（セイイチ）啊，还是一如既往的早起啊，难道想要做对练吗？”
看来正好是空挥（素振り）结束吧，发现了我的丹邀请我训练了。


“不了，今天还是稍微停下休息一下吧。比起这个，我受伤了……像治疗室一样的地方在哪裡？”
听了我的话丹一副担心的样子，


“嗯？仔细看的话这不是很重的伤吗。我带您去。”
说着就把肩膀借给了我。真是个好人啊。好人啊……可以的话希望能把汗擦干再过来啊，只是这样看起来更闷热了。总觉得这样粘着心情很不好。


“啊，非常感谢”
“没什麼，不要在意啦。我也被委托了勇者的照料这些事。”


之後两人都无言的样子走着，忽然丹像是想起什麼似的问了诚一。


“这麼说起来，向自己的女仆打听的话医务室的地方也能够告诉你的吧？为什麼还特意到这裡来找我呢？”


啊，确实是啊。有女仆的话就是这样子的啊。


“实际上，只有我一个人没有安排女仆，说是人手不足了什麼的……”
因为不太想让这个话被多少人知道所以说的比较模糊。


“啊……那个公主大人的恶习又犯了吗。”
像是有什麼可惜的样子单丹嘟囔着。而且，那个的意思是说那个可恶的王女一直都是这样子的吗。


“不过嘛最开始遇到的时候發生了些口角，状态也比周围的同伴们的低所以没有安排女仆，住的房间什麼的也在王城的外面的破房子裡。不会平时也是那样的感觉吧？”
知道的王女的恶性不知不觉就说出来了。我真的有打算把这当做秘密保存起来吗？


“嘛，那个人只是稍微有些难以相处的地方……平常也是为国事而考虑的很好的人哦……有什麼不方便的话要对我说吗？我会提供些帮助的。哦呀，这裡就是医务室了。毕竟是王城的医务室嘛，有高位的回復魔法的使用者在安全应该是有保证的吧？”


我和丹在装饰漂亮的门前停了下来。说到这个世界的治疗的话，是使用回復魔法和药草或药水之类的药类的东西占大部分。创可贴（绊创膏）之类的应该是没有的吧。


走进去後裡面是位穿着豪华衣服的神父桑。


“哦呀，治疗是吗？”
看见了借着丹的肩膀进来的我，神父桑这麼问了。


“啊，这傢伙的伤请治疗一下，相当严重的伤啊。”
“明白了。那麼坐到那边去。”


神父桑看了看我的腹部皱了皱眉。
“这可真是相当的严重啊。治疗需要一些时间所以就这样放鬆的姿势做好。”


“嗯，这个很严重吗？”
我相当的不安呐。


“不，确实很严重但并不是治不好所以请放心吧。”
神父桑像是要我放心似的安慰我。这裡也和现代的医生也是一样的。


“失礼了……鼓动的生命啊 治疗汝之伤痛 给予汝生命的气息 『高级治疗（ハイヒール）』”
突然间感觉腹部的附近被能使心情变好的温暖感包围了疼痛也减轻了。也许这就是（受到）魔法的感觉吧。


经过三十秒左右後光消失了。疼痛感也幾乎不剩什麼了，但神父依然在咏唱。


“治疗汝之伤痛……『治疗（ヒール）』”
正想着的时候温暖再次包围了我，剩下的那点疼痛也不见了。


“这样治疗就结束了。”
“果然奥尔特（オルト）的回復魔法就是厲害啊。高级治疗的咏唱缩短到极限了啊。”
“非常感谢您”


神父桑——我对奥尔特先生道了谢。或许刚才所说的高级治疗和咏唱缩短什麼的是很厲害的技术吧。


“不不，没有那麼厲害的。世上也有使用特级治疗（エクストラヒール）的人啊，和那种人相比我这可完全不行了……”


看来奥尔特先生很谦虚啊。特级治疗之类的。听名字就挺猛的，不知道可不可以让我拜学一下啊。王城都没有的吧。


“不不，十分的厲害啊。非常感谢您。”


我们準备离开医务室的时候再一次的致谢。这之後丹说要去洗澡。我的话由於昨天战鬥了的缘故，所以暂时分别去拿换洗的衣服後再去洗澡了。


“好的，到那裡也没有多少时间赶快去洗澡吧不过……说起来昨天打倒的史莱姆的掉落物是怎麼样的东西呢？”


在去洗澡之前我确认了昨天的战利品是什麼东西。最坏，再没用也比哥布林这种弱小的魔物的掉落物值得期待吧，大概吧。


我走到昨天达到史莱姆的地方，史莱姆的掉落物还在地上滚动着。看来是不会被魔物拿走的那样。


史莱姆的掉落物品确认了是比哥布林的魔石更大的那种，有拳头大小的魔石和像果冻一样的东西——球体的什麼落下来了、谨慎起见我发动了鉴定。


【鉴定】！


[史莱姆果冻
史莱姆掉落的果冻
有毒故人无法使用]


[史莱姆的魔石（4）
史莱姆略低概率掉落的魔石可被合成东西
可注入魔力但超过一定量会损坏]


[史莱姆的核
满足特殊条件打到史莱姆时低概率的史莱姆会掉落。
覆盖複数个体的史莱姆果冻可以降生使役状态下的史莱姆]


好像是史莱姆掉落物应该没问题的。『被合成东西』很在意这个啊，不过这个史莱姆的等级很高的吧？从我最初的状态就能打倒这个了啊。


看到这个掉落物我就想起了昨天的战鬥。老实说昨天的那个相当危险啊。还好打倒了这个高等级的。再说原本史莱姆这个种族就是个弱小的我是这麼推测的。如果是像奥克那样的强大的种族的话反而会是LV.90被LV.50的奥克反打回去。


我把战利品裹在替换衣服裡隐藏起来，走到裸体的丹等待的地方……也就是洗澡的地方。




我从浴场出来後就去了食堂找大地搭话。


“那个诚一，这是怎麼了？昨天没来吃晚饭吧”
“啊啊，稍微被外面的史莱姆袭击了”


听我这麼说的大地一边吃着饭一边小声说着。我可没有说谎哦。


“史莱姆？是史莱姆的史莱姆吗？很强吗？”
“不，史莱姆的话是孩子也能踢飞就死的杂鱼那样的吗？”
“嗯怎麼了吗？”


对着不可思议的想着的大地我稍微考虑後说。


“稍微那个和我战鬥的傢伙等级很高，陷入了相当的苦战啊”
“苦战？让你陷入苦战的程度有多高啊？”


就大地而言我被5个人狠狠的欺负这样的事担心是多余的（就是说男主1打5没问题）


“因为是你才说但别对他人说行吗？……LV.90”
我也想要值得信赖的伙伴什麼的。大地的话可信，也已经听了我的话了。说的也没问题。


“咦！？那个……厲害吗？”


不由得颤抖了一下。毕竟那些傢伙是魔物，有这样的反应才是妥当的。


“不 详细的我也说不好大概和リ○グボール１号的苍蓝的速度冲撞的强度相当……我觉得”【原文：いや俺も详しくはわかんねぇけど大リ○グボール１号も真っ青なスピードで体当たりしてきたし多分かなり强い……と思う】


我也不知道普通的史莱姆是什麼等级的，也不认为时速200km行动的史莱姆能被孩子踢飞，那傢伙是异常的吧。


“即便这样你还是赢了……果然你是异常的”
“是吗？我只是一个普通的高中生啊。话说我才是最吃惊的吧。”


真亏我没死啊。【原文：本当によく死ななかったよな俺。】


========================================================
作者：大リ○グボール1号は花形が打ち返した奴で2号が水に弱い奴で3号は……どんな奴だっけ？【白字：不懂啊，作者用的梗。顺便说一句 本话完】
