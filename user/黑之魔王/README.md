# novel

- title: 黒の魔王
- author: 菱影代理
- source: http://ncode.syosetu.com/n2627t/
- cover: https://2.bp.blogspot.com/-qG7giNZTRuI/VkpMgj44IfI/AAAAAAAAAL4/MldUKuehBaE/s1600/volumen-1-0.jpg
- status: 連載

## series

- name: 黒の魔王

## preface

```
黑乃真央是一个有着凶恶眼神的男高中生。没有女友的他每天仍能享受着和平的高中生活。但是有一天，在没有预兆的情况下黑乃在他所属的文艺部的活动室，头突然痛了起来，然后晕了过去。当他其次醒来的时候等待他的到底是什么……。
```

## tags

- syosetu
- 异界
- 穿越
- 冒险
- 后宫
- 病嬌
- BL
- R15
- シリアス
- ファンタジー
- ヤンデレ
- 冒険者
- 国家
- 民族
- 架空戦記
- 残酷な描写あり
- 異世界
- 異世界転移
- 魔法
- 魔王

# contribute

- SenpaiSugar
- 灵九条
- 永恒
- 悠久之回忆
- ScarletFlash
- GYAZZA
- 异端贝的
- arqrwrer
- 雪梦緣
- cfws00
- 陨月天碎
- tsehoyin
- 海市蜃楼谁不识
- 玩脱的秀吉
- Elvis233
- 萤火奇点
- terry12369
- 帅气金龟
- 无尽神行靴
- 呕吐的蛋糕
- 歌利亚
- 圣斗士小翔
- LY
- 和郑西EX
- yukari
- 夏洛特sky
- 萝莉・最高
- 嘴角荡笑
- 娜娜
- fhqwerty乐园
- 海德特尔沃
- retaw2
- wraith4404
- 成萌新这件事
- kong5sl
- 延2000
- 零号机403
- daohaocao4
- I法克灬油
- redsouris
- LV4歌利亞
- 嘴角蕩笑
- 把傷疤當做酒窩
- 歪空の姬
- gnh07
- predatorwzy
- 沛之暗影
- celestia173
- 

# options

## textlayout

- allow_lf2: false

## dmzj

- novel_id: 2367

## syosetu

- txtdownload_id: 192608

## qq

### id

- 616844565

# link

- [黑之魔王.ts](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%BB%91%E4%B9%8B%E9%AD%94%E7%8E%8B.ts)
- [dip.jp](https://narou.dip.jp/search.php?text=n2627t&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [黒之魔王吧](https://tieba.baidu.com/f?kw=%E9%BB%92%E4%B9%8B%E9%AD%94%E7%8E%8B&ie=utf-8 "")
- [灵九条](https://pan.baidu.com/s/1pMi5zXT) d3xv
- [masiro.moe](https://masiro.moe/forum.php?mod=forumdisplay&fid=53&page=1)
- [黒の魔王・裏](http://novel18.syosetu.com/n9884df/)
- 
