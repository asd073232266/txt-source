# CONTENTS

Genocide Online ～極惡千金的玩遊戲日記～  
ジェノサイド・オンライン 〜極悪令嬢のプレイ日記〜  

作者： たけのこ  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/girl/out/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/girl/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/girl/Genocide%20Online%20～極惡千金的玩遊戲日記～/導航目錄.md "導航目錄")




## [null](00000_null)

- [序章](00000_null/00010_%E5%BA%8F%E7%AB%A0.txt)
- [1.角色塑造](00000_null/00020_1.%E8%A7%92%E8%89%B2%E5%A1%91%E9%80%A0.txt)
- [2.教程叔叔殺人事件](00000_null/00030_2.%E6%95%99%E7%A8%8B%E5%8F%94%E5%8F%94%E6%AE%BA%E4%BA%BA%E4%BA%8B%E4%BB%B6.txt)
- [3.對神不敬的惡行](00000_null/00040_3.%E5%B0%8D%E7%A5%9E%E4%B8%8D%E6%95%AC%E7%9A%84%E6%83%A1%E8%A1%8C.txt)
- [4.最終戦果](00000_null/00050_4.%E6%9C%80%E7%B5%82%E6%88%A6%E6%9E%9C.txt)
- [5.地下墳墓探索](00000_null/00060_5.%E5%9C%B0%E4%B8%8B%E5%A2%B3%E5%A2%93%E6%8E%A2%E7%B4%A2.txt)
- [6.地下墳墓探索其2](00000_null/00070_6.%E5%9C%B0%E4%B8%8B%E5%A2%B3%E5%A2%93%E6%8E%A2%E7%B4%A2%E5%85%B62.txt)
- [7.地下墳墓探索其3](00000_null/00080_7.%E5%9C%B0%E4%B8%8B%E5%A2%B3%E5%A2%93%E6%8E%A2%E7%B4%A2%E5%85%B63.txt)
- [8.地下墳墓探索其4](00000_null/00090_8.%E5%9C%B0%E4%B8%8B%E5%A2%B3%E5%A2%93%E6%8E%A2%E7%B4%A2%E5%85%B64.txt)
- [9.地下墳墓探索其5](00000_null/00100_9.%E5%9C%B0%E4%B8%8B%E5%A2%B3%E5%A2%93%E6%8E%A2%E7%B4%A2%E5%85%B65.txt)
- [10.地下墳墓探索其6](00000_null/00110_10.%E5%9C%B0%E4%B8%8B%E5%A2%B3%E5%A2%93%E6%8E%A2%E7%B4%A2%E5%85%B66.txt)
- [11.公式掲示板](00000_null/00120_11.%E5%85%AC%E5%BC%8F%E6%8E%B2%E7%A4%BA%E6%9D%BF.txt)
- [12.暗中活動](00000_null/00130_12.%E6%9A%97%E4%B8%AD%E6%B4%BB%E5%8B%95.txt)
- [13.暗中活動其2](00000_null/00140_13.%E6%9A%97%E4%B8%AD%E6%B4%BB%E5%8B%95%E5%85%B62.txt)
- [14.暗中活動其3](00000_null/00150_14.%E6%9A%97%E4%B8%AD%E6%B4%BB%E5%8B%95%E5%85%B63.txt)
- [15.暗中活動其4](00000_null/00160_15.%E6%9A%97%E4%B8%AD%E6%B4%BB%E5%8B%95%E5%85%B64.txt)
- [16.付諸行動](00000_null/00170_16.%E4%BB%98%E8%AB%B8%E8%A1%8C%E5%8B%95.txt)
- [17.付諸行動その2](00000_null/00180_17.%E4%BB%98%E8%AB%B8%E8%A1%8C%E5%8B%95%E3%81%9D%E3%81%AE2.txt)
- [18.付諸行動その3](00000_null/00190_18.%E4%BB%98%E8%AB%B8%E8%A1%8C%E5%8B%95%E3%81%9D%E3%81%AE3.txt)
- [19.亞歷克西・瓦朗斯](00000_null/00200_19.%E4%BA%9E%E6%AD%B7%E5%85%8B%E8%A5%BF%E3%83%BB%E7%93%A6%E6%9C%97%E6%96%AF.txt)
- [20.亞歷克西・瓦朗斯其2](00000_null/00210_20.%E4%BA%9E%E6%AD%B7%E5%85%8B%E8%A5%BF%E3%83%BB%E7%93%A6%E6%9C%97%E6%96%AF%E5%85%B62.txt)

