# HISTORY

## 2019-02-20

### Epub

- [姫騎士がクラスメート！　〜異世界チートで奴隷化ハーレム〜](cm/%E5%A7%AB%E9%A8%8E%E5%A3%AB%E3%81%8C%E3%82%AF%E3%83%A9%E3%82%B9%E3%83%A1%E3%83%BC%E3%83%88%EF%BC%81%E3%80%80%E3%80%9C%E7%95%B0%E4%B8%96%E7%95%8C%E3%83%81%E3%83%BC%E3%83%88%E3%81%A7%E5%A5%B4%E9%9A%B7%E5%8C%96%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%80%9C) - cm
  <br/>( v: 4 , c: 77, add: 0 )
- [王国へ続く道](cm/%E7%8E%8B%E5%9B%BD%E3%81%B8%E7%B6%9A%E3%81%8F%E9%81%93) - cm
  <br/>( v: 1 , c: 4, add: 2 )
- [你這種傢伙別想打贏魔王](girl_out/%E4%BD%A0%E9%80%99%E7%A8%AE%E5%82%A2%E4%BC%99%E5%88%A5%E6%83%B3%E6%89%93%E8%B4%8F%E9%AD%94%E7%8E%8B) - girl_out
  <br/>( v: 16 , c: 112, add: 1 )
- [陰の実力者になりたくて！_(n0611em)](syosetu_out/%E9%99%B0%E3%81%AE%E5%AE%9F%E5%8A%9B%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%9F%E3%81%8F%E3%81%A6%EF%BC%81_(n0611em)) - syosetu_out
  <br/>( v: 9 , c: 179, add: 9 )

### Segment

- [王国へ続く道](cm/%E7%8E%8B%E5%9B%BD%E3%81%B8%E7%B6%9A%E3%81%8F%E9%81%93) - cm
  <br/>( s: 1 )
- [你這種傢伙別想打贏魔王](girl/%E4%BD%A0%E9%80%99%E7%A8%AE%E5%82%A2%E4%BC%99%E5%88%A5%E6%83%B3%E6%89%93%E8%B4%8F%E9%AD%94%E7%8E%8B) - girl
  <br/>( s: 4 )
- [陰の実力者になりたくて！_(n0611em)](syosetu/%E9%99%B0%E3%81%AE%E5%AE%9F%E5%8A%9B%E8%80%85%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%9F%E3%81%8F%E3%81%A6%EF%BC%81_(n0611em)) - syosetu
  <br/>( s: 3 )

## 2019-02-19

### Epub

- [王国へ続く道](cm/%E7%8E%8B%E5%9B%BD%E3%81%B8%E7%B6%9A%E3%81%8F%E9%81%93) - cm
  <br/>( v: 1 , c: 2, add: 1 )
- [Free Life Fantasy Online ～人外姫様、始めました～](girl/Free%20Life%20Fantasy%20Online%20%EF%BD%9E%E4%BA%BA%E5%A4%96%E5%A7%AB%E6%A7%98%E3%80%81%E5%A7%8B%E3%82%81%E3%81%BE%E3%81%97%E3%81%9F%EF%BD%9E) - girl
  <br/>( v: 2 , c: 9, add: 9 )
- [今度こそ幸せになります！](girl/%E4%BB%8A%E5%BA%A6%E3%81%93%E3%81%9D%E5%B9%B8%E3%81%9B%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%BE%E3%81%99%EF%BC%81) - girl
  <br/>( v: 1 , c: 6, add: 6 )
- [你這種傢伙別想打贏魔王](girl_out/%E4%BD%A0%E9%80%99%E7%A8%AE%E5%82%A2%E4%BC%99%E5%88%A5%E6%83%B3%E6%89%93%E8%B4%8F%E9%AD%94%E7%8E%8B) - girl_out
  <br/>( v: 16 , c: 111, add: 111 )
- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 27 , c: 457, add: 1 )
- [転生ごときで逃げられるとでも、兄さん？](yandere_out/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere_out
  <br/>( v: 5 , c: 84, add: 1 )

### Segment

- [Free Life Fantasy Online ～人外姫様、始めました～](girl/Free%20Life%20Fantasy%20Online%20%EF%BD%9E%E4%BA%BA%E5%A4%96%E5%A7%AB%E6%A7%98%E3%80%81%E5%A7%8B%E3%82%81%E3%81%BE%E3%81%97%E3%81%9F%EF%BD%9E) - girl
  <br/>( s: 3 )
- [今度こそ幸せになります！](girl/%E4%BB%8A%E5%BA%A6%E3%81%93%E3%81%9D%E5%B9%B8%E3%81%9B%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%BE%E3%81%99%EF%BC%81) - girl
  <br/>( s: 6 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user
  <br/>( s: 1 )
- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 1 )

## 2019-02-18

### Epub

- [王国へ続く道](cm/%E7%8E%8B%E5%9B%BD%E3%81%B8%E7%B6%9A%E3%81%8F%E9%81%93) - cm
  <br/>( v: 1 , c: 1, add: 1 )
- [最強呪族転生](syosetu_out/%E6%9C%80%E5%BC%B7%E5%91%AA%E6%97%8F%E8%BB%A2%E7%94%9F) - syosetu_out
  <br/>( v: 6 , c: 230, add: 9 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user_out/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user_out
  <br/>( v: 7 , c: 184, add: 0 )

### Segment

- [豚公爵に転生したから、今度は君に好きと言いたい](user/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user
  <br/>( s: 1 )

## 2019-02-17

### Epub

- [姫騎士がクラスメート！　〜異世界チートで奴隷化ハーレム〜](cm/%E5%A7%AB%E9%A8%8E%E5%A3%AB%E3%81%8C%E3%82%AF%E3%83%A9%E3%82%B9%E3%83%A1%E3%83%BC%E3%83%88%EF%BC%81%E3%80%80%E3%80%9C%E7%95%B0%E4%B8%96%E7%95%8C%E3%83%81%E3%83%BC%E3%83%88%E3%81%A7%E5%A5%B4%E9%9A%B7%E5%8C%96%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%80%9C) - cm
  <br/>( v: 4 , c: 77, add: 2 )
- [リビティウム皇国のブタクサ姫](cm_out/%E3%83%AA%E3%83%93%E3%83%86%E3%82%A3%E3%82%A6%E3%83%A0%E7%9A%87%E5%9B%BD%E3%81%AE%E3%83%96%E3%82%BF%E3%82%AF%E3%82%B5%E5%A7%AB) - cm_out
  <br/>( v: 7 , c: 214, add: 1 )
- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( v: 1 , c: 21, add: 6 )
- [異世界迷宮の最深部を目指そう](user_out/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E3%81%AE%E6%9C%80%E6%B7%B1%E9%83%A8%E3%82%92%E7%9B%AE%E6%8C%87%E3%81%9D%E3%81%86) - user_out
  <br/>( v: 28 , c: 393, add: 7 )
- [自称贤者弟子的贤者](user_out/%E8%87%AA%E7%A7%B0%E8%B4%A4%E8%80%85%E5%BC%9F%E5%AD%90%E7%9A%84%E8%B4%A4%E8%80%85) - user_out
  <br/>( v: 7 , c: 301, add: 2 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user_out/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user_out
  <br/>( v: 7 , c: 184, add: 1 )
- [魔王様、リトライ！](user_out/%E9%AD%94%E7%8E%8B%E6%A7%98%E3%80%81%E3%83%AA%E3%83%88%E3%83%A9%E3%82%A4%EF%BC%81) - user_out
  <br/>( v: 9 , c: 118, add: 2 )
- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 27 , c: 456, add: 0 )
- [転生ごときで逃げられるとでも、兄さん？](yandere_out/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere_out
  <br/>( v: 5 , c: 83, add: 2 )

### Segment

- [リビティウム皇国のブタクサ姫](cm/%E3%83%AA%E3%83%93%E3%83%86%E3%82%A3%E3%82%A6%E3%83%A0%E7%9A%87%E5%9B%BD%E3%81%AE%E3%83%96%E3%82%BF%E3%82%AF%E3%82%B5%E5%A7%AB) - cm
  <br/>( s: 3 )
- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( s: 4 )
- [異世界迷宮の最深部を目指そう](user/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E3%81%AE%E6%9C%80%E6%B7%B1%E9%83%A8%E3%82%92%E7%9B%AE%E6%8C%87%E3%81%9D%E3%81%86) - user
  <br/>( s: 191 )
- [自称贤者弟子的贤者](user/%E8%87%AA%E7%A7%B0%E8%B4%A4%E8%80%85%E5%BC%9F%E5%AD%90%E7%9A%84%E8%B4%A4%E8%80%85) - user
  <br/>( s: 9 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user
  <br/>( s: 2 )
- [魔王様、リトライ！](user/%E9%AD%94%E7%8E%8B%E6%A7%98%E3%80%81%E3%83%AA%E3%83%88%E3%83%A9%E3%82%A4%EF%BC%81) - user
  <br/>( s: 24 )
- [転生ごときで逃げられるとでも、兄さん？](yandere/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere
  <br/>( s: 2 )

## 2019-02-16

### Epub

- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 27 , c: 456, add: 0 )

### Segment

- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 1 )

## 2019-02-15

### Epub

- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 27 , c: 456, add: 0 )

### Segment

- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 1 )

## 2019-02-13

### Epub

- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 27 , c: 456, add: 0 )

### Segment

- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 4 )



